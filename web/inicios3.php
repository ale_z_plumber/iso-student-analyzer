<?php
session_start();
if( (! isset($_SESSION['promocion'])) || (! $_GET['servidor']) || (! $_GET['alumno']) ){header('location:inicios1.php');};
$promocion=$_SESSION['promocion'];
$servidor=$_GET['servidor'];
$alumno=$_GET['alumno'];

$bd = new SQLite3('/var/iso-student-analyzer/bbdd.db');
$bd->enableExceptions(true);
$sentencia = $bd->prepare("SELECT STRFTIME('<b>%Y-%m-%d</b> a las <b>%H:%M:%S</b>',DATETIME(momento, 'unixepoch')) as 'momento', duracion FROM IniciosSesion WHERE alumno=:alumno AND servidor=:servidor ORDER BY momento");
$sentencia->bindValue(':alumno', $alumno);
$sentencia->bindValue(':servidor', $servidor);
$sentencia->bindValue(':momento', $momento);
$sentencia->bindValue(':duracion', $duracion);
$resultado = $sentencia->execute();

?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="css.css">
    <title>Iso Student Analyzer</title>
    <style>
        td{
            padding: 10px;
            border-color: rgb(196, 194, 194);
            border-width: 1px;
            border-style: solid;
            text-align:center;
        }
        div{
            padding-left:0%;
        }
    </style>
</head>
<body>
<header>
        <div style="padding-left:0%;"><center><a href="indice.php"><img src="img/banner.png" width="35%"></a></center></div>
        <div><a href="cerrar.php"><img src="img/cerrar.png" style="position:absolute; right:2%;top:2%"></a></div>
</header>
<div class="padre">
            <?php
            echo '<center><p>Inicios de sesión del alumno<b style="font-weight:550;"> '.$alumno.'</b> en <b style="font-weight:550;">'.$servidor.'</b></p></center>';
            ?>
            
<div><center><table style="margin-top:2rem;">
    <tr><td><b>Momento del inicio de sesión</b></td><td><b>minutos de sesión</b></td></tr>
<?php
while ($fila = $resultado->fetchArray()) {
    $momento=$fila['momento'];
    $duracion=$fila['duracion'];
    echo '<tr><td>'.$momento.'</td><td>'.$duracion.'</td></tr>';
}
?>
</table></center></div>

<footer style="padding:0; float: none; clear: both; background: #ccc; text-align: center; line-height: 3.5; position: relative;top:3.5rem;">
    © <?= date('Y') ?> Copyright:
	<a class="text-dark" href="indice.php">ISO Student Analyzer, creado por Alejandro 2º ASIR</a>
	</footer>
</body>
</html>