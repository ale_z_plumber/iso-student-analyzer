<?php
session_start();
#Pruebas preliminares
if( ! isset($_SESSION['promocion']) ){header('location:index.html');};
$promocion=$_SESSION['promocion'];

$bd = new SQLite3('/var/iso-student-analyzer/bbdd.db');
$bd->enableExceptions(true);
# Servidores multiusuario
$sentencia_ser_multi = $bd->prepare("SELECT nombre FROM Servidor WHERE monouser=FALSE");
$sentencia_ser_multi->bindValue(':nombre', $nombre);
$resultado_ser_multi = $sentencia_ser_multi->execute();


$servidores_multi=array();
while ($fila_ser_multi = $resultado_ser_multi->fetchArray()) {
    $servidor=$fila_ser_multi['nombre'];
    array_push($servidores_multi, $servidor);
}
$servidores_multi_long=count($servidores_multi);
# Servidores monousuario
$sentencia_ser_mono = $bd->prepare("SELECT nombre FROM Servidor WHERE monouser=TRUE");
$sentencia_ser_mono->bindValue(':nombre', $nombre);
$resultado_ser_mono = $sentencia_ser_mono->execute();


$servidores_mono=array();
while ($fila_ser_mono = $resultado_ser_mono->fetchArray()) {
    $servidor=$fila_ser_mono['nombre'];
    array_push($servidores_mono, $servidor);
}
$servidores_mono_long=count($servidores_mono);

?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="css.css">
    <title>Iso Student Analyzer</title>
    <style>
        .cardsfoto{
            padding-bottom:20px;
            padding-top:7px;
        }
        .cards{
            margin: 2%;
        }
        .boton_cards{
            width:10.6rem;
            border-color: rgb(139, 139, 139);
            border-width: 1px;
            border-style: solid;
        }
	.padre {
		float: none;
		clear: both;
	}
    </style>
</head>
<body>

<header>
        <div style="padding-left:0%;"><center><a href="indice.php"><img src="img/banner.png" width="35%"></a></center></div>
        <div><a href="cerrar.php"><img src="img/cerrar.png" style="position:absolute; right:2%;top:2%"></a></div>
        <center><div style="margin-top:1.5rem;"><h4>Listado de los servidores disponibles</h4></div></center>
</header>
    
    <?php
    if ( ($servidores_multi_long==0) && ($servidores_mono_long==0) ) {
        echo '<p>No se han encontrado servidores disponibles</p>';
    } else {
        if ($servidores_multi_long > 0) {
            echo '<div class="padre"><h5 style="margin-top:1rem; margin-left:1rem;">Servidores Multiusuario</h5>';
            for ($x=0; $x < $servidores_multi_long; $x++) { 
                $servidor=$servidores_multi[$x];
                # Obtener ejercicios en ese servidor.
                $sentencia_nejercicio = $bd->prepare("SELECT nombre FROM Ejercicio WHERE servidor=:servidor");
                $sentencia_nejercicio->bindValue(':nombre', $nombre);
                $sentencia_nejercicio->bindValue(':servidor', $servidor);
                $resultado_nejercicio = $sentencia_nejercicio->execute();


                $ejercicios=array();
                while ($fila_nejercicios = $resultado_nejercicio->fetchArray()) {
                    $ejercicio=$fila_nejercicios['nombre'];
                    array_push($ejercicios, $ejercicio);
                }
                $nejercicios=count($ejercicios);

                # IP del servidor
                $sentencia_ip = $bd->prepare("SELECT direccion FROM Servidor WHERE nombre=:servidor");
                $sentencia_ip->bindValue(':direccion', $direccion);
                $sentencia_ip->bindValue(':servidor', $servidor);
                $resultado_ip = $sentencia_ip->execute();
                $fila_ip = $resultado_ip->fetchArray();
                $ip=$fila_ip['direccion'];

                #Alumnos que trabajaron
                $sentencia_nalumnos = $bd->prepare("SELECT COUNT(DISTINCT(alumno)) as 'nalumnos' FROM IniciosSesion WHERE Servidor=:servidor AND alumno LIKE '$promocion%'");
                $sentencia_nalumnos->bindValue(':nalumnos', $nalumnos);
                $sentencia_nalumnos->bindValue(':servidor', $servidor);
                $resultado_nalumnos = $sentencia_nalumnos->execute();
                $fila_nalumnos = $resultado_nalumnos->fetchArray();
                $nalumnos=$fila_nalumnos['nalumnos'];


                echo'<div class="card hijo pad cards" style="width: 19.5rem;">
                    <div class="card-body">
                        <center><h5 class="card-title" style="font-size:23px;margin-bottom:1.5rem;">'.$servidor.'</h5></center>
                        <table><tr><td><b style="font-weight:500;">Dirección IP:</b></td><td style="padding-left:2rem;">'.$ip.'</td></tr>
                        <tr><td><b style="font-weight:500;">Ejercicios:</b></td><td style="padding-left:4.6rem;">'.$nejercicios.'</td></tr>
                        <tr><td><b style="font-weight:500;">Logins: </b></td><td style="padding-left:4.3rem;">'.$nalumnos.'</td></tr>
                        </table></div></div>';
            }
            echo '</div>';
        }
        if ($servidores_mono_long > 0) {
            echo '<div class="padre"><h5 style="margin-left:1rem;">Servidores Monousuario</h5>';
            for ($x=0; $x < $servidores_mono_long; $x++) { 
                $servidor=$servidores_mono[$x];
                # Obtener ejercicios en ese servidor.
                $sentencia_nejercicio = $bd->prepare("SELECT nombre FROM Ejercicio WHERE servidor=:servidor");
                $sentencia_nejercicio->bindValue(':nombre', $nombre);
                $sentencia_nejercicio->bindValue(':servidor', $servidor);
                $resultado_nejercicio = $sentencia_nejercicio->execute();


                $ejercicios=array();
                while ($fila_nejercicios = $resultado_nejercicio->fetchArray()) {
                    $ejercicio=$fila_nejercicios['nombre'];
                    array_push($ejercicios, $ejercicio);
                }
                $nejercicios=count($ejercicios);

                # Alumnos con inicios de sesion
                $sentencia_logins = $bd->prepare("SELECT COUNT(DISTINCT(alumno)) as 'nlogins' FROM IniciosSesion WHERE Servidor=:servidor AND alumno LIKE '$promocion%'");
                $sentencia_logins->bindValue(':nlogins', $nlogins);
                $sentencia_logins->bindValue(':servidor', $servidor);
                $resultado_logins = $sentencia_logins->execute();
                $fila_logins = $resultado_logins->fetchArray();
                $nlogins=$fila_logins['nlogins'];

                #Alumnos añadidos al servidor
                $sentencia_nalumnos = $bd->prepare("SELECT count(DISTINCT(alumno)) as 'nalumnos' FROM ServidorMono WHERE Servidor=:servidor AND alumno LIKE '$promocion%'");
                $sentencia_nalumnos->bindValue(':nalumnos', $nalumnos);
                $sentencia_nalumnos->bindValue(':servidor', $servidor);
                $resultado_nalumnos = $sentencia_nalumnos->execute();
                $fila_nalumnos = $resultado_nalumnos->fetchArray();
                $nalumnos=$fila_nalumnos['nalumnos'];


                echo'<div class="card hijo pad cards" style="width: 14rem;">
                    <div class="card-body">
                        <center><h5 class="card-title" style="font-size:23px;margin-bottom:1.5rem;">'.$servidor.'</h5></center>
                        <table><tr><td><b style="font-weight:500;">Alumnos: </b></td><td style="padding-left:2rem;">'.$nalumnos.'</td></tr>
                        <tr><td><b style="font-weight:500;">Ejercicios:</b></td><td style="padding-left:2rem;">'.$nejercicios.'</td></tr>
                        <tr><td><b style="font-weight:500;">Logins:</b></td><td style="padding-left:2rem;">'.$nlogins.'</td></tr>
                        </table>
                        
                    </div></div>';
            }
            echo "</div>";
        }
    }
    
    ?>
    <footer style="padding:0; float: none; clear: both; background: #ccc; text-align: center; line-height: 3.5; position: relative;top:3.5rem;">
    © <?= date('Y') ?> Copyright:
	<a class="text-dark" href="indice.php">ISO Student Analyzer, creado por Alejandro 2º ASIR</a>
	</footer>
</body>
</html>
