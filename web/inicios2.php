<?php
session_start();
if( (! isset($_SESSION['promocion'])) || (! $_GET['servidor']) ){header('location:inicios1.php');};
$promocion=$_SESSION['promocion'];
$servidor=$_GET['servidor'];

$bd = new SQLite3('/var/iso-student-analyzer/bbdd.db');
$bd->enableExceptions(true);
$sentencia = $bd->prepare("SELECT DISTINCT alumno FROM IniciosSesion WHERE alumno LIKE '$promocion%' AND servidor=:servidor ORDER BY alumno");
$sentencia->bindValue(':alumno', $alumno);
$sentencia->bindValue(':servidor', $servidor);
$resultado = $sentencia->execute();


$alumnos=array();
while ($fila = $resultado->fetchArray()) {
    $usu_test=$fila['alumno'];
    array_push($alumnos, $usu_test);
}
$alumnos_long=count($alumnos);

?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="css.css">
    <title>Iso Student Analyzer</title>
    <style>
        .botones{
            width:12rem;
            height: 4rem;
            text-align: center;
            padding-top:1.1rem;
        }
    </style>
</head>
<body>
<header>
        <div style="padding-left:0%;"><center><a href="indice.php"><img src="img/banner.png" width="35%"></a></center></div>
        <div><a href="cerrar.php"><img src="img/cerrar.png" style="position:absolute; right:2%;top:2%"></a></div>
</header>
<div class="padre">
            <center><p>Selecciona un servidor del que buscar inicios de sesión</p></center>
            
<div class="hijo">
<?php
if ($alumnos_long == 0) {
    echo '<div style="padding:5rem;""><p style="font-size:20px;">No se han encontrado inicios de sesión en este servidor</p></div>';
} else {
    for ($i=0; $i < $alumnos_long; $i++) { 
        echo'<div class="card hijo pad" style="width: 16rem;">
        <div class="card-body">
            <h5 class="card-title"></h5>
            <a href="inicios3.php?servidor='.$servidor.'&alumno='.$alumnos[$i].'" class="btn btn-outline-primary botones">'.$alumnos[$i].'</a>
        </div>
    </div>';
    }
}
?>
</div></div>


<footer style="padding:0; float: none; clear: both; background: #ccc; text-align: center; line-height: 3.5; position: relative;top:3.5rem;">
    © <?= date('Y') ?> Copyright:
	<a class="text-dark" href="indice.php">ISO Student Analyzer, creado por Alejandro 2º ASIR</a>
	</footer>
</body>
</html>