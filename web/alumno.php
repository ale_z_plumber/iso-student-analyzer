<?php
session_start();
#Pruebas preliminares
if( ! isset($_SESSION['promocion']) ){header('location:index.html');};
if (! $_GET) {
    header('location:alumnos.php');
}
else{
    $promocion=$_SESSION['promocion'];
    $alumn=$_GET['alumno'];
    $bd = new SQLite3('/var/iso-student-analyzer/bbdd.db');
    $bd->enableExceptions(true);
    $sentencia = $bd->prepare("SELECT usuario FROM Alumno WHERE usuario LIKE '$promocion%'");
    $sentencia->bindValue(':usuario', $usuario);
    $resultado = $sentencia->execute();
    
    $users=array();
    while ($fila = $resultado->fetchArray()) {
        $alumno=$fila['usuario'];
        array_push($users, $alumno);
    }
    if (!in_array($alumn, $users)) {
        header('location:alumnos.php');
    }


    }

?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="css.css">
    <title>Iso Student Analyzer</title>
    <style>
        .listausuarios{
            color:black;
            padding-right:1rem;
        }
        .bordeusuarios{
            border-right-color: rgb(156, 156, 156);
            border-right-width: 1px;
            border-right-style: dashed;
        }
        a:link{
            color:black;
            text-decoration:none;
        }
        a:visited{
            color:black;
            text-decoration:none;
        }
        a:hover{
            color:black;
            text-decoration:none;
        }
        a:active{
            color:black;
            text-decoration:none;
        }

    div.server-container {
        border-right: 1px solid #ccc;
    }
    div.server-container:last-child {
        border-right: 0;
    }
    div.server-container table tr > td:last-child {
        text-align: right;
        width: 4rem;
    }

    </style>
</head>
<body>
<header>
        <div style="padding-left:0%;"><center><a href="indice.php"><img src="img/banner.png" width="35%"></a></center></div>
        <div><a href="cerrar.php"><img src="img/cerrar.png" style="position:absolute; right:2%;top:2%"></a></div>
</header>
<div class="padre">
    
        <div class="hijo" style="border-color:green;">
        <table class=table>
<?php
#Muestra todos los usuarios
$users_long=count($users);
for($x=0;$x<$users_long;$x++){
    echo '<tr class="bordeusuarios"><td><a href=alumno.php?alumno='.$users[$x].'><div class="listausuarios">'.$users[$x].'</div></a></td></tr>';
}
?>

</table></div>
<div>
<?php
#Escribe el nombre del alumno
echo '<h1 style="padding-left:10rem;text-decoration: underline solid rgb(165, 161, 161);">'.$alumn.'</h1>';
?>
</div>
<div style="padding-left:7.5rem;margin"><div><h3 style="margin-bottom:1rem;">Ejercicios</h3></div><table><tr><td class='tablasguapas'>Ejercicio</td><td class='tablasguapas'>Nota</td><td class='tablasguapas'>Comentarios</td></tr>
<?php
    # EJERCICIOS

    $sentencia = $bd->prepare("SELECT ejercicio, nota, comentario FROM AlumnoEjercicio WHERE alumno='$alumn'");
    $sentencia->bindValue(':ejercicio', $ejercicio);
    $sentencia->bindValue(':nota', $nota);
    $sentencia->bindValue(':comentario', $comentario);
    $resultado = $sentencia->execute();
    
    $users=array();
    while ($fila = $resultado->fetchArray()) {
        echo '<tr><td class="tablasguapas">'.$fila['ejercicio'].'</td><td class="tablasguapas">'.$fila['nota'].'</td><td class="tablasguapas">'.$fila['comentario'].'</td></tr>';
        $alumno=$fila['usuario'];
        if (str_starts_with($alumno, $promocion)) {
            array_push($users, $alumno);
        }
    }
?></table></div>



<?php

# LOGS
    # Obtiene servidores
    $sentencia_servidores = $bd->prepare("SELECT DISTINCT servidor from IniciosSesion WHERE alumno=:alumn");
    $sentencia_servidores->bindValue(':alumn', $alumn);
    $sentencia_servidores->bindValue(':servidor', $servidor);
    $resultado_servidores = $sentencia_servidores->execute();
    
    $servidores=array();
    
    while ($filaservidores = $resultado_servidores->fetchArray()) {
        $servidor=$filaservidores['servidor'];
        array_push($servidores,$servidor);
        
    }
    $servidores_long=count($servidores);
    if ($servidores_long > 0) {
        echo '<div style="padding-bottom:0rem;padding-left:7.5rem;margin-top:0.8rem;"><h3>Resúmen de inicios de sesión</h3>';
    } else {
        echo '<div style="padding-bottom:0rem;padding-left:7.5rem;margin-top:0.8rem;"><h3>Resúmen de inicios de sesión</h3><p>No se ha encontrado información de inicio de sesión</p>';
    }
    for($x=0;$x<$servidores_long;$x++){
        $servidor=$servidores[$x];
        # Obtiene el número de dias distintos que trabaja el alumno
        $sentencia_ndias = $bd->prepare("SELECT count(DISTINCT(DATE(momento, 'unixepoch'))) as 'dias_distintos' from IniciosSesion WHERE alumno=:alumno AND servidor=:servidor;");
        $sentencia_ndias->bindValue(':alumno', $alumn);
        $sentencia_ndias->bindValue(':servidor', $servidor);
        $sentencia_ndias->bindValue(':dias_distintos', $dias_distintos);
        $resultado = $sentencia_ndias->execute();
        $fila_ndias = $resultado->fetchArray();
        $dias_distintos=$fila_ndias['dias_distintos'];
        
        # Obtiene la media que trabaja al dia
        $sentencia_mediadias = $bd->prepare("SELECT AVG(media) as 'media' FROM (SELECT SUM(duracion) as 'media' FROM IniciosSesion WHERE alumno=:alumno AND servidor=:servidor GROUP BY DATE(momento, 'unixepoch'));");
        $sentencia_mediadias->bindValue(':media', $media);
        $sentencia_mediadias->bindValue(':alumno', $alumn);
        $sentencia_mediadias->bindValue(':servidor', $servidor);
        $resultado = $sentencia_mediadias->execute();
        $fila_mediadias = $resultado->fetchArray();
        $mediadias=round($fila_mediadias['media']);

        # Obtiene el numero de inicios de sesion en clase
        $sentencia_iniciosclase = $bd->prepare("SELECT count(*) as 'enclase' FROM IniciosSesion WHERE time(momento,'unixepoch') < '14:45:00' AND time(momento,'unixepoch') > '08:15:00' AND servidor=:servidor AND alumno=:alumn AND duracion > 9;");
        $sentencia_iniciosclase->bindValue(':enclase', $enclase);
        $sentencia_iniciosclase->bindValue(':alumn', $alumn);
        $sentencia_iniciosclase->bindValue(':servidor', $servidor);
        $resultado = $sentencia_iniciosclase->execute();
        $fila_iniciosclase = $resultado->fetchArray();
        $iniciosclase=$fila_iniciosclase['enclase'];

        # numero de veces totales que trabaja
        $sentencia_vecestotal = $bd->prepare("SELECT count(*) as 'total' FROM IniciosSesion WHERE servidor=:servidor AND alumno=:alumn AND duracion > 9;");
        $sentencia_vecestotal->bindValue(':total', $total);
        $sentencia_vecestotal->bindValue(':alumn', $alumn);
        $sentencia_vecestotal->bindValue(':servidor', $servidor);
        $resultado = $sentencia_vecestotal->execute();
        $fila_vecestotal = $resultado->fetchArray();
        $vecestotal=$fila_vecestotal['total'];
        if ($vecestotal==0) {
            $porcentajecasa='-';
        } elseif ($iniciosclase==0) {
            $porcentajecasa=100;
        } else {
            $porcentajecasa=round(100-($iniciosclase / $vecestotal * 100));
        }



        echo '<div class="server-container" style="display: inline-block; width: 21.5rem; height: auto;"><h5>'.$servidor.'</h5>';
        echo '<table style="margin: 0.15rem 1rem 0.15rem 2rem;"><tr><td>Número de días trabajados:</td><td>'.$dias_distintos.'</td></tr>';
        echo '<tr><td>Minutos de media al día:</td><td>'.$mediadias.'</td></tr>';
        echo '<tr><td>Tiempo de trabajo en casa:</td><td>'.$porcentajecasa.'%</td></tr></table>';
        echo '<a href="inicios3.php?servidor='.$servidor.'&alumno='.$alumn.'">Ver todos</a></div>';
    }
?>
</div>



<div style="padding-left:7.5rem;margin-top:1rem;"><h3>Resúmen de comandos ejecutados</h3><table class="tablasguapas">
    <p>Comandos más ejecutados</p>
<?php

# Comandos

    $sentencia = $bd->prepare("SELECT comando, veces FROM AlumnoComando WHERE alumno='$alumn' ORDER BY veces DESC LIMIT 25");
    $sentencia->bindValue(':comando', $comando);
    $sentencia->bindValue(':veces', $veces);
    $resultado = $sentencia->execute();
    $t1='<tr><td class="tablasguapas">Comando</td>';
    $t2='<tr><td class="tablasguapas">Ejecuciones</td>';
    $users=array();
    while ($fila = $resultado->fetchArray()) {
        $t1=$t1."<td class='tablasguapas'>".$fila['comando']."</td>";
        $t2=$t2."<td class='tablasguapas'>".$fila['veces']."</td>";
        $alumno=$fila['usuario'];
        if (str_starts_with($alumno, $promocion)) {
            array_push($users, $alumno);
        }
    }
    echo $t1.'</tr>';
    echo $t2.'</tr>';
?></table>
<br>
<?php
echo '<a href="comandosalumn.php?alumno='.$alumn.'">Ver todos</a>';
?>
</div>
</div>

    <footer style="padding:0; float: none; clear: both; background: #ccc; text-align: center; line-height: 3.5;">
    © <?= date('Y') ?> Copyright:
	<a class="text-dark" href="indice.php">ISO Student Analyzer, creado por Alejandro 2º ASIR</a>
	</footer>
</body>
</html>
