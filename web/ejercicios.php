<?php
session_start();
#Pruebas preliminares
$promocion=$_SESSION['promocion'];

if( ! isset($_SESSION['promocion']) ){header('location:index.html');};
$bd = new SQLite3('/var/iso-student-analyzer/bbdd.db');
$bd->enableExceptions(true);
$sentencia = $bd->prepare("SELECT usuario FROM Alumno WHERE usuario LIKE '$promocion%' ORDER BY usuario");
$sentencia->bindValue(':usuario', $usuario);
$resultado = $sentencia->execute();


$users=array();
while ($fila = $resultado->fetchArray()) {
    #var_dump($fila['usuario']);
    $usu_test=$fila['usuario'];
    if (str_starts_with($usu_test, $promocion)) {
        array_push($users, $usu_test);
    }

}


?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="css.css">
    <title>Iso Student Analyzer</title>
    <style>
        td{
            padding: 10px;
            border-color: rgb(196, 194, 194);
            border-width: 1px;
            border-style: solid;
            text-align:center;
        }
    </style>
</head>
<body>
<header>
        <div style="padding-left:0%;"><center><a href="indice.php"><img src="img/banner.png" width="35%"></a></center></div>
        <div><a href="cerrar.php"><img src="img/cerrar.png" style="position:absolute; right:2%;top:2%"></a></div>
</header>
<div class="padre">
        <h3 style="padding-left:2rem;margin-top:2rem;">Listado de ejercicios realizados por los alumnos de la promocion</h3>
        <br>
<?php
#Muestra todos los usuarios
$users_long=count($users);

?>

</table></div>

<div style="padding-left:2rem;overflow: scroll;"><table class="table">

<?php
# Obtiene alumnos
$linea1='<tr><td style="border-width: 0px;border-style: solid;border-color: white);"></td>';
for($x=0;$x<$users_long;$x++){
    $linea1=$linea1.'<td><b>'.$users[$x].'</b></td>';
}
echo $linea1;

# Obtiene ejercicios
$sentencia = $bd->prepare("SELECT nombre FROM Ejercicio");
$sentencia->bindValue(':nombre', $nombre);
$resultado = $sentencia->execute();
$ejercicios=array();
while ($fila = $resultado->fetchArray()) {
    $nombre=$fila['nombre'];
    array_push($ejercicios, $nombre);
}
$ejercicios_long=count($ejercicios);
for($x=0;$x<$ejercicios_long;$x++){
    $ejercicio=$ejercicios[$x];
    $t1="<tr><td><b>".$ejercicio."</b></td>";
    for ($y=0; $y < $users_long; $y++) { 
        $usu_actual=$users[$y];
        $sentencia = $bd->prepare("SELECT nota FROM AlumnoEjercicio WHERE ejercicio='$ejercicio' AND alumno='$usu_actual'");
        $sentencia->bindValue(':nota', $nota);
        $resultado = $sentencia->execute();
        $salida_nota = $resultado->fetchArray();
        $t1=$t1.'<td>'.$salida_nota['nota'].'</td>';
    }
    echo $t1; 
    
}
?>
</table></div>

<footer class="bg-light text-center text-lg-start" style="position: absolute;bottom: 0rem; width: 100%; height: 3.5rem">
        <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2); height: 100%;">
        © <?= date('Y') ?> Copyright:
            <a class="text-dark" href="indice.php">ISO Student Analyzer, creado por Alejandro 2º ASIR</a>
        </div>
        </footer>
    </div></div>

</body>
</html>