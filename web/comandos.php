<?php
session_start();
#Pruebas preliminares
if( ! isset($_SESSION['promocion']) ){header('location:index.html');};
$promocion=$_SESSION['promocion'];

$bd = new SQLite3('/var/iso-student-analyzer/bbdd.db');
$bd->enableExceptions(true);
$sentencia = $bd->prepare("SELECT usuario FROM Alumno WHERE usuario LIKE '$promocion%' ORDER BY usuario");
$sentencia->bindValue(':usuario', $usuario);
$resultado = $sentencia->execute();


$users=array();
while ($fila = $resultado->fetchArray()) {
    $usu_test=$fila['usuario'];
    array_push($users, $usu_test);
}


?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="css.css">
    <title>Iso Student Analyzer</title>
    <style>
        td{
            padding: 10px;
            border-color: rgb(196, 194, 194);
            border-width: 1px;
            border-style: solid;
            text-align:center;
        }
    </style>
</head>
<body>
<header>
        <div style="padding-left:0%;"><center><a href="indice.php"><img src="img/banner.png" width="35%"></a></center></div>
        <div><a href="cerrar.php"><img src="img/cerrar.png" style="position:absolute; right:2%;top:2%"></a></div>
</header>
<div class="padre">
        <br>
        <h3 style="margin-top:1rem;padding-left:2rem;">Listado de comandos ejecutados por los alumnos de la promocion</h3>
        <br>
<?php
#Muestra todos los usuarios
$users_long=count($users);

?>

</table></div>

<div style="padding-left:1rem;padding-right:1rem;margin-bottom:2rem;overflow: scroll;"><table class="table">

<?php
# Obtiene alumnos
$linea1='<tr><td style="border-width: 0px;border-style: solid;border-color: white);"></td>';
for($x=0;$x<$users_long;$x++){
    $linea1=$linea1.'<td><b>'.$users[$x].'</b></td>';
}
echo $linea1;

# Obtiene comandos
$sentencia = $bd->prepare("SELECT nombre FROM Comando ORDER BY nombre");
$sentencia->bindValue(':nombre', $nombre);
$resultado = $sentencia->execute();
$comandos=array();
while ($fila = $resultado->fetchArray()) {
    $nombre=$fila['nombre'];
    array_push($comandos, $nombre);
}
$comandos_long=count($comandos);
for($x=0;$x<$comandos_long;$x++){
    $comando=$comandos[$x];
    $t1="<tr><td>".$comando."</td>";
    $sentencia = $bd->prepare("SELECT alumno, veces FROM AlumnoComando WHERE comando='$comando' ORDER BY alumno");
    $sentencia->bindValue(':alumno', $alumno);
    $sentencia->bindValue(':veces', $veces);
    $resultado = $sentencia->execute();

    
    while ($fila = $resultado->fetchArray()) {

    $alumno=$fila['alumno'];
    if (str_starts_with($alumno, $promocion)) {
        $t1=$t1."<td>".$fila['veces']."</td>";
    }
    }
    echo $t1;
}
?>
</table></div>

<div style="padding-left:0%">
<footer class="bg-light text-center text-lg-start">
        <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2); height: 100%;">
        © <?= date('Y') ?> Copyright:
            <a class="text-dark" href="indice.php">ISO Student Analyzer, creado por Alejandro 2º ASIR</a>
        </div>
        </footer>
        </div>
    </div>
</body>
</html>