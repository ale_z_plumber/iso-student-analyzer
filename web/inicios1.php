<?php
session_start();
if( ! isset($_SESSION['promocion']) ){header('location:index.html');};
$promocion=$_SESSION['promocion'];

$bd = new SQLite3('/var/iso-student-analyzer/bbdd.db');
$bd->enableExceptions(true);
# Obtiene servidores
$sentencia_servidores = $bd->prepare("SELECT DISTINCT servidor from IniciosSesion WHERE alumno LIKE '$promocion%'");
$sentencia_servidores->bindValue(':servidor', $servidor);
$resultado_servidores = $sentencia_servidores->execute();

$servidores=array();

while ($filaservidores = $resultado_servidores->fetchArray()) {
    $servidor=$filaservidores['servidor'];
    array_push($servidores,$servidor);
    
}
$servidores_long=count($servidores);


?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="css.css">
    <title>Iso Student Analyzer</title>
    <style>
        .botones{
            width:12rem;
            height: 4rem;
            text-align: center;
            padding-top:1.1rem;
        }
    </style>
</head>
<body>
<header>
        <div style="padding-left:0%;"><center><a href="indice.php"><img src="img/banner.png" width="35%"></a></center></div>
        <div><a href="cerrar.php"><img src="img/cerrar.png" style="position:absolute; right:2%;top:2%"></a></div>
</header>
<div class="padre">
            <center><p>Selecciona un servidor del que buscar inicios de sesión</p></center>
            
<div class="hijo">
<?php
if ($servidores_long == 0) {
    echo '<div><p> No se han encontrado servidores</p></div>';
} else {
    for ($i=0; $i < $servidores_long; $i++) { 
        echo'<div class="card hijo pad" style="width: 16rem;">
        <div class="card-body">
            <h5 class="card-title"></h5>
            <a href="inicios2.php?servidor='.$servidores[$i].'" class="btn btn-outline-primary botones">'.$servidores[$i].'</a>
        </div>
    </div>';
    }
}

?>
</div>
</div>

<footer class="bg-light text-center text-lg-start" style="position: absolute;bottom: 0rem; width: 100%; height: 3.5rem">
        <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2); height: 100%;">
        © <?= date('Y') ?> Copyright:
            <a class="text-dark" href="indice.php">ISO Student Analyzer, creado por Alejandro 2º ASIR</a>
        </div>
        </footer>

</body>
</html>