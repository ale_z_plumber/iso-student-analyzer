<?php
session_start();
if( ! isset($_SESSION['promocion']) ){header('location:index.html');};
$promocion=$_SESSION['promocion'];
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="css.css">
    <title>Iso Student Analyzer</title>
    <style>
        .botones{
            width:12rem;
            height: 4rem;
            text-align: center;
            padding-top:1.1rem;
        }
    </style>
</head>
<body>
    <header>
        <div style="padding-left:0%;"><center><a href="indice.php"><img src="img/banner.png" width="35%"></a></center></div>
        <div><a href="cerrar.php"><img src="img/cerrar.png" style="position:absolute; right:2%;top:2%"></a></div>
</header>
<div class="padre">
        <div class="padre">
            <div><h3 style="margin-top:1.5rem;margin-bottom:2rem;padding-left:1rem;">Escoge un alumno</h3></div>
<?php
$bd = new SQLite3('/var/iso-student-analyzer/bbdd.db');
$bd->enableExceptions(true);
$sentencia = $bd->prepare("SELECT usuario FROM Alumno WHERE usuario LIKE '$promocion%' ORDER BY usuario");
$sentencia->bindValue(':usuario', $usuario);
$resultado = $sentencia->execute();


$users=array();
while ($fila = $resultado->fetchArray()) {
    #var_dump($fila['usuario']);
    $alumno=$fila['usuario'];
    array_push($users, $alumno);
}
$users_long=count($users);
for($x=0;$x<$users_long;$x++){
    echo'<div class="card hijo pad" style="width: 16rem;">
    <div class="card-body">
        <h5 class="card-title"></h5>
        <a href="alumno.php?alumno='.$users[$x].'" class="btn btn-outline-primary botones">'.$users[$x].'</a>
    </div>
</div>';
}
?>



        </div>
    </div>



    <footer style="padding:0; float: none; clear: both; background: #ccc; text-align: center; line-height: 3.5; position: relative;top:3.5rem;">
    © <?= date('Y') ?> Copyright:
	<a class="text-dark" href="indice.php">ISO Student Analyzer, creado por Alejandro 2º ASIR</a>
	</footer>
</body>
</html>