<?php
session_start();
if( ! isset($_SESSION['promocion']) ){header('location:index.html');};
$promocion=$_SESSION['promocion'];
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="fontawesome/css/all.min.css">
    <link rel="stylesheet" href="css.css">
    <title>Iso Student Analyzer</title>
    <style>
        .cardsfoto{
            padding-bottom:20px;
            padding-top:7px;
        }
        .cards{
            margin: 2%;
        }
        .boton_cards{
            width:10.6rem;
            border-color: rgb(139, 139, 139);
            border-width: 1px;
            border-style: solid;
        }
    </style>
</head>
<body>
<header>
        <div style="padding-left:0%;"><center><a href="indice.php"><img src="img/banner.png" width="35%"></a></center></div>
        <div><a href="cerrar.php"><img src="img/cerrar.png" style="position:absolute; right:2%;top:2%"></a></div>
</header>
<div class="padre">


            <div class="card hijo pad cards" style="width: 15rem;">
            <center><i class="fas fa-user-graduate fa-5x cardsfoto"></i></center>
                <div class="card-body">
                    <center><h5 class="card-title">Alumnos</h5></center>
                    <p class="card-text" style="text-align: justify;text-justify: inter-word;">Muestra un resumen del trabajo de cada alumno de la promoción</p>
                    <a href="alumnos.php" class="btn btn-light boton_cards">Ver</a>
                </div>
            </div>
            <div class="card hijo pad cards" style="width: 15rem;">
                <center><i class="fas fa-file fa-5x cardsfoto"></i></center>
                <div class="card-body">
                    <center><h5 class="card-title">Ejercicios</h5></center>
                    <p class="card-text" style="text-align: justify;text-justify: inter-word;">Contiene las notas de todos los alumnos de la promoción en cada ejercicio</p>
                    <a href="ejercicios.php" class="btn btn-light boton_cards">Ver</a>
                </div>
            </div>
            <div class="card hijo pad card cards" style="width: 15rem;">
                <center><i class="fas fa-sign-in-alt fa-5x cardsfoto"></i></center>
                <div class="card-body">
                    <h5 class="card-title">Inicios de sesión</h5>
                    <p class="card-text" style="text-align: justify;text-justify: inter-word;">Muestra un resúmen de los inicios de sesión de cada alumno de la promoción</p>
                    <a href="inicios1.php" class="btn btn-light boton_cards">Ver</a>
                </div>
            </div>
            <div class="card hijo pad cards" style="width: 15rem;">
            <center><i class="fas fa-terminal fa-5x cardsfoto"></i></center>
                <div class="card-body">
                    <center><h5 class="card-title">Comandos</h5></center>
                    <p class="card-text" style="text-align: justify;text-justify: inter-word;">Lista completa de los comandos ejecutados por cada alumno de la promoción</p>
                    <a href="comandos.php" class="btn btn-light boton_cards">Ver</a>
                </div>
            </div>
            <div class="card hijo pad cards" style="width: 15rem;">
            <center><i class="fas fa-server fa-5x cardsfoto"style="padding-bottom:10px;"></i></center>
                <div class="card-body">
                    <center><h5 class="card-title">Servidores</h5></center>
                    <p class="card-text" style="text-align: justify;text-justify: inter-word;">Información individual de cada uno de los servidores recogidos por la aplicación</p>
                    <a href="servidores.php" class="btn btn-light boton_cards">Ver</a>
                </div>
            </div>



        </div>
    </div>



    <footer class="bg-light text-center text-lg-start" style="position: absolute;bottom: 0rem; width: 100%; height: 3.5rem">
        <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2); height: 100%;">
        © <?= date('Y') ?> Copyright:
            <a class="text-dark" href="indice.php">ISO Student Analyzer, creado por Alejandro 2º ASIR</a>
        </div>
        </footer>
</body>
</html>