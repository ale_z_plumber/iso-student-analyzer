<?php
session_start();
#Pruebas preliminares
if( ! isset($_SESSION['promocion']) ){header('location:index.html');};
if (!$_GET['alumno']) {
    header("location:alumnos.php");
}
$promocion=$_SESSION['promocion'];

$alumno=$_GET['alumno'];
$bd = new SQLite3('/var/iso-student-analyzer/bbdd.db');
$bd->enableExceptions(true);
$sentencia = $bd->prepare("SELECT usuario FROM Alumno WHERE usuario LIKE '$promocion%' ORDER BY usuario");
$sentencia->bindValue(':usuario', $usuario);
$resultado = $sentencia->execute();


$users=array();
while ($fila = $resultado->fetchArray()) {
    $usu_test=$fila['usuario'];
    array_push($users, $usu_test);
}

if (! in_array($alumno, $users)) {
    header("location:alumnos.php");
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="css.css">
    <title>Iso Student Analyzer</title>
    <style>
        .listausuarios{
            color:black;
            padding-right:1rem;
        }
        .bordeusuarios{
            border-right-color: rgb(156, 156, 156);
            border-right-width: 1px;
            border-right-style: dashed;
        }
        a:link{
            color:black;
            text-decoration:none;
        }
        a:visited{
            color:black;
            text-decoration:none;
        }
        a:hover{
            color:black;
            text-decoration:none;
        }
        a:active{
            color:black;
            text-decoration:none;
        }

	.comandobloque {
		display: inline-block;
		background: #0af;
		border-radius: 0.5rem;
		margin: 0.5rem;
		line-height: 1.5;
		padding: 0 0 0 0.5rem;
		color: white;
	}
	.comandobloque div {
		display: inline-block;
	}
	.comandobloque .comando {
		width: 6rem;
		font-family: monospace;
	}
	.comandobloque .usos {
		width: 3rem;
		text-align: right;
		background: #08f;
		border-radius: 0.5rem;
		padding-right: 0.5rem;
	}
    </style>
</head>
<body>
    <header>
        <div style="padding-left:0%;"><center><a href="indice.php"><img src="img/banner.png" width="35%"></a></center></div>
        <div><a href="cerrar.php"><img src="img/cerrar.png" style="position:absolute; right:2%;top:2%"></a></div>
    </header>
        <div class="padre" style="float: none; clear: both;">
<?php
echo '<h3 style="margin-left:4rem;margin-top:1rem;margin-bottom:1rem;">Listado de comandos/usos ejecutados por el alumno '.$alumno.'</h3>';

#Muestra todos los usuarios
$users_long=count($users);
echo'<div class="hijo" style="width: 6rem;"><table class=table>';
for($x=0;$x<$users_long;$x++){
    echo '<tr class="bordeusuarios"><td><a href=comandosalumn.php?alumno='.$users[$x].'>'.$users[$x].'</td></tr>';
}
?>

</table></div>

<div class="hijo" style="width: calc(100% - 6rem);">

<?php
$sentencia = $bd->prepare("SELECT comando, veces FROM AlumnoComando WHERE alumno='$alumno' ORDER BY veces DESC");
$sentencia->bindValue(':comando', $comando);
$sentencia->bindValue(':veces', $veces);
$resultado = $sentencia->execute();

while ($fila = $resultado->fetchArray()) {
    $comando=$fila['comando'];
    $veces=$fila['veces'];
    echo '<div class="comandobloque"><div class="comando">'.$comando.'</div><div class="usos">'.$veces.'</div></div>';
}
echo $t1;
?>

</div>

    </div>
<footer class="bg-light text-center text-lg-start" style="float: none; clear: both;">
        <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2); height: 100%;">
        © <?= date('Y') ?> Copyright:
            <a class="text-dark" href="indice.php">ISO Student Analyzer, creado por Alejandro 2º ASIR</a>
        </div>
        </footer>
</body>
</html>
