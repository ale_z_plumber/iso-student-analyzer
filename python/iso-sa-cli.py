#!/usr/bin/env python3
from argparse import ArgumentParser
import os, yaml, sqlite3, tarfile, paramiko, datetime, utmp
from pathlib import Path
from getpass import getpass
from functions import *

#-----------------------------
# LINEA DE COMANDOS

def rutina_diaria(args):
    print("Ejecutando rutina diaria")
    if not os.path.isfile("/var/iso-student-analyzer/bbdd.db"):
        print("No se encontró la Base de Datos, se creará de nuevo")
        bbdd_creation()

    # Busca y añade usuarios
    print("Añadiendo alumnos a la Base de Datos")
    user_add()

    # Busca y añade servidores a la BBDD
    print("Obteniendo servidores")
    for server in server_get():
        server_add(server)

    # Comprueba los comandos ejecutados por los alumnos.
    print("Analizando logs de los alumnos")
    for alumno in user_get():
        for tar in buscalogs(alumno):
            leetar(alumno, tar)

    # Busca y añade los ejercicios a la BBDD
    print("Buscando ejercicios para añadir a la Base de Datos")
    find_ejs()

    # Corrige ejercicios
    print("Corrigiendo ejercicios multiusuario")
    ej_check_multi()

    # Actualiza los inicios de sisión en máquinas multiusuario
    print("Obteniendo inicios de sesión en servidores multiusuario")
    get_multi_logins()

def reset(args):
    permiso = False
    if args.force == 'f' or args.force == 'force':
        permiso = True
        print("Opción force especificada. Se va a borrar la BBDD")
    else:
        entrada_usu = input("¿Está seguro de que quiere borrar la BBDD y volver a crearla?\n¡¡Se perderán todos los datos!!\nEscriba [s|y] para borrar, cualquier otra entrada cancelará la operación: ")
        if entrada_usu == 's' or entrada_usu == 'y':
            permiso = True

    if permiso == True:
        print("Reiniciando la BBDD")
        if os.path.isfile("/var/iso-student-analyzer/bbdd.db"):
            print("Base de Datos encontrada, se procederá a su eliminación")
            os.remove('/var/iso-student-analyzer/bbdd.db')
        bbdd_creation()

        # Busca y añade usuarios
        print("Añadiendo alumnos a la Base de Datos")
        user_add()

        # Busca y añade servidores a la BBDD
        print("Obteniendo servidores")
        for server in server_get():
            server_add(server)

        # Comprueba los comandos ejecutados por los alumnos.
        print("Analizando logs de los alumnos")
        for alumno in user_get():
            for tar in buscalogs(alumno):
                leetar(alumno, tar)

        # Busca y añade los ejercicios a la BBDD
        print("Buscando ejercicios para añadir a la Base de Datos")
        find_ejs()

        # Corrige ejercicios
        print("Corrigiendo ejercicios multiusuario")
        ej_check_multi()

        # Actualiza los inicios de sisión en máquinas multiusuario
        print("Obteniendo inicios de sesión en servidores multiusuario")
        get_multi_logins()

def busca_ejs(args):
    for ejercicio in ej_get_yaml():
        ej_add(ejercicio)

def corrige_ejs(args):
    alumnos = user_get()
    if args.ejercicio == None:
        # En caso de que se invoque en vacío corrige todos los multiusuario
        ej_check_multi()
    else:
        try:
            con = sqlite3.connect("/var/iso-student-analyzer/bbdd.db")
            cursor = con.cursor()
            res = cursor.execute(f"SELECT monouser FROM Servidor LEFT JOIN Ejercicio ON Ejercicio.servidor=Servidor.nombre WHERE Ejercicio.nombre=?;",(args.ejercicio,))
            tipo = res.fetchone()
        except:
            print("Error al buscar el ejercicio")
            tipo = None
        finally:
            cursor.close()
            con.close()
        if not tipo:
            print("No se ha encontrado ningún ejercicio con ese nombre en la BBDD, si fue añadido recientemente pruebe a ejecutar el subcomando ej find")
        else:
            if not args.alumno and tipo[0] == 0:
                # sin alumno, multiusuario
                print(f"Corrigiendo ej ejercicio {args.ejercicio} para todos los alumnos...")
                ej_exec_multi(ej_get_multi(args.ejercicio)[0])
            if not args.alumno and tipo[0] == 1:
                # sin alumno, monousuario
                print(f"Corrigiendo ejercicio {args.ejercicio} para todos los alumnos...")
                for entrada in ej_get_mono(args.ejercicio):
                    if entrada[1] in alumnos:
                        ej_exec_mono(entrada)
            if args.alumno and tipo[0] == 0:
                # con alumno, multiusuario
                print(f"Corrigiendo el ejercicio {args.ejercicio} para todos los alumnos...")
                ej_exec_multi(ej_get_multi(args.ejercicio)[0])
            if args.alumno and tipo[0] == 1:
                # con alumno, monousuario
                print(f"Corrigiendo ejercicio {args.ejercicio} para el alumno {args.alumno}...")
                marca = False
                for entrada in ej_get_mono(args.ejercicio):
                    if args.alumno == entrada[1] and args.alumno in alumnos:
                        ej_exec_mono(entrada)
                        marca = True
                if marca == False:
                    print(f"No se ha encontrado ninguna entrada para el alumno {args.alumno} en el servidor con ip {entrada[4]}")

def reset_ej(args):
    ejercicio = args.ejercicio
    alumno = args.alumno
    alumnos = user_get()

    if not ejercicio and not alumno:
        entrada_usu = input("¿Está seguro de que quiere Eliminar todas las notas para todos los ejercicios?\n¡¡Esta acción no se puede deshacer!!\nEscriba [s|y] para borrar, cualquier otra entrada cancelará la operación: ")
        if entrada_usu == 's' or entrada_usu == "y":
            con = sqlite3.connect("/var/iso-student-analyzer/bbdd.db")
            cursor = con.cursor()
            res = cursor.execute(f"SELECT DISTINCT alumno FROM AlumnoEjercicio")
            alumnos_bd = res.fetchall()
            try:
                for alu in alumnos_bd:
                    alu = alu[0]
                    if alu in alumnos:
                        cursor.execute(f"UPDATE AlumnoEjercicio SET nota=0, comentario='Sin corregir.'WHERE alumno=?;",(alu,))
                        con.commit()
                print("Notas reiniciadas correctamente")
            except:
                print("Ha ocurrido un error al reiniciar la notas")
            finally:
                cursor.close()
                con.close()
        else:
            print("Operación abortada")
    else:
        if not alumno:
            con = sqlite3.connect("/var/iso-student-analyzer/bbdd.db")
            cursor = con.cursor()
            res = cursor.execute(f"SELECT DISTINCT alumno FROM AlumnoEjercicio WHERE ejercicio=?",(ejercicio,))
            alumnos_bd = res.fetchall()
            try:
                for alu in alumnos_bd:
                    alu = alu[0]
                    if alu in alumnos:
                        cursor.execute(f"UPDATE AlumnoEjercicio SET nota=0, comentario='Sin corregir.'WHERE alumno=? AND ejercicio=?;",(alu,ejercicio))
                        con.commit()
            except:
                print(f"Ha ocurrido un error al reiniciar las notas en el ejercicio {ejercicio}")
        else:
            con = sqlite3.connect("/var/iso-student-analyzer/bbdd.db")
            cursor = con.cursor()
            res = cursor.execute(f"SELECT alumno FROM AlumnoEjercicio WHERE ejercicio=? AND alumno=?",(ejercicio,alumno))
            if not res.fetchone():
                print(f"No se ha encontrado ninguna entrada para el alumno {alumno} en el ejercicio {ejercicio}")
            try:
                if alumno in alumnos:
                    cursor.execute(f"UPDATE AlumnoEjercicio SET nota=0, comentario='Sin corregir.'WHERE alumno=? AND ejercicio=?;",(alumno,ejercicio))
                    con.commit()
            except:
                print(f"Ha ocurrido un error al reiniciar la nota de {alumno} en el ejercicio {ejercicio}")



def borrar_ejercicio(args):
    con = sqlite3.connect("/var/iso-student-analyzer/bbdd.db")
    cursor = con.cursor()
    cursor.execute("PRAGMA foreign_keys = ON")
    con.commit()
    res = cursor.execute(f"SELECT nombre FROM Ejercicio WHERE nombre=?",(args.ejercicio,))
    if not res.fetchone():
        print("El ejercicio no existe.")
    else:
        try:
            print(f'Borrando ejercicio {args.ejercicio}')
            res = cursor.execute(f"DELETE FROM Ejercicio WHERE nombre=?;",(args.ejercicio,))
            con.commit()
            print(f"Ejercicio {args.ejercicio} eliminado correctamente.")
        except:
            print(f"Error al eliminar el ejercicio {args.ejercicio}.")
            tipo = None
    cursor.close()
    con.close()

def borrar_alumno(args):
    con = sqlite3.connect("/var/iso-student-analyzer/bbdd.db")
    cursor = con.cursor()
    cursor.execute("PRAGMA foreign_keys = ON")
    con.commit()
    res = cursor.execute(f"SELECT usuario FROM Alumno WHERE usuario=?",(args.alumno,))
    if not res.fetchone():
        print("El alumno no existe.")
    else:
        try:
            print(f'Borrando alumno {args.alumno}')
            res = cursor.execute(f"DELETE FROM Alumno WHERE usuario=?;",(args.alumno,))
            con.commit()
            print(f"Alumno {args.alumno} eliminado correctamente.")
        except:
            print(f"Error al eliminar el alumno {args.alumno}.")
            tipo = None
    cursor.close()
    con.close()

def busca_alumnos(args):
    user_add()

def busca_servidores(args):
    for server in server_get():
        server_add(server)

def borrar_servidor(args):
    permiso = input("¿Seguro que quieres eliminar este servidor?\nSe va a eliminar toda la información de inicio de sesión del mismo, así como los ejercicios y sus correcciones que dependen de el\nEscribe [s|y] para confirmar. Cualquier otra cosa para denegar: ")
    if permiso == "s" or permiso == "y":
        con = sqlite3.connect("/var/iso-student-analyzer/bbdd.db")
        cursor = con.cursor()
        cursor.execute("PRAGMA foreign_keys = ON")
        con.commit()
        res = cursor.execute(f"SELECT nombre FROM Servidor WHERE nombre=?",(args.servidor,))
        if not res.fetchone():
            print("El servidor no existe.")
        else:
            try:
                print(f'Borrando servidor {args.servidor}')
                res = cursor.execute(f"DELETE FROM Servidor WHERE nombre=?;",(args.servidor,))
                con.commit()
                print(f"Servidor {args.servidor} eliminado correctamente.")
            except:
                print(f"Error al eliminar el servidor {args.servidor}.")
                tipo = None
        cursor.close()
        con.close()
    else:
        print("Operación abortada.")


def muestra_contrasena(args):
    con = sqlite3.connect("/var/iso-student-analyzer/bbdd.db")
    cursor = con.cursor()
    res = cursor.execute(f"SELECT clave FROM Accesos WHERE promocion=?",(args.promocion,))
    contra = res.fetchone()
    if not contra:
        print("La promoción especificada no existe")
    elif not contra[0]:
        print("La promoción no tiene contraseña")
    else:
        print(f"la contraseña de la promoción {args.promocion} es")
        print(contra[0])
    cursor.close()
    con.close()


def cambiar_contrasena(args):
    con = sqlite3.connect("/var/iso-student-analyzer/bbdd.db")
    cursor = con.cursor()
    res = cursor.execute(f"SELECT clave FROM Accesos WHERE promocion=?",(args.promocion,))
    contra = res.fetchone()
    if not contra:
        print("La promoción especificada no existe")
    else:
        if not args.contraseña:
            try:
                p = getpass()
            except Exception as error:
                print('ERROR', error)
        else:
            p = args.contraseña
        if p != '' and p != ' ' and len(p) < 128:
            res = cursor.execute(f"UPDATE Accesos SET clave=? WHERE promocion=?;",(p,args.promocion))
            con.commit()
        else:
            print("No se puede colocar una contraseña vacía, con un único espacio o de más de 128 caracteres")
    cursor.close()
    con.close()
        
def borrar_contrasena(args):
    con = sqlite3.connect("/var/iso-student-analyzer/bbdd.db")
    cursor = con.cursor()
    res = cursor.execute(f"SELECT clave FROM Accesos WHERE promocion=?",(args.promocion,))
    if not res.fetchone():
        print("La promoción especificada no existe")
    else:
        try:
            res = cursor.execute(f"UPDATE Accesos SET clave=NULL WHERE promocion=?;",(args.promocion,))
            con.commit()
        except:
            print("Error al borrar la contraseña")
    cursor.close()
    con.close()

def inicios_check(args):

    alumnos = user_get()
    if not args.servidor:
        # En caso de que se invoque en vacío corrige todos los multiusuario
        print("Obteniendo inicios de sesión en servidores multiusuario")
        get_multi_logins()
    else:
        con = sqlite3.connect("/var/iso-student-analyzer/bbdd.db")
        cursor = con.cursor()
        res = cursor.execute(f"SELECT monouser FROM Servidor WHERE nombre=?",(args.servidor,))
        mono = res.fetchone()
        if not mono:
            print("El servidor no existe")
        elif not args.alumno:
            if mono[0] == 0:
                # Es multiuser
                get_multi_server_logins([args.servidor])
            else:
                # Es monouser
                for entrada in alumn_server_get_mono(args.servidor):
                    alumn_server_calc_mono(entrada)
                #
        else:
            # Se han pasado servidor y alumno
            if mono[0] == 0:
                get_multi_server_logins([args.servidor])
            else:
                for entrada in alumn_server_get_mono(args.servidor):
                    if entrada[1] == args.alumno and args.alumno in alumnos:
                        alumn_server_calc_mono(entrada)


def inicios_borrar(args):
    con = sqlite3.connect("/var/iso-student-analyzer/bbdd.db")
    cursor = con.cursor()
    res = cursor.execute(f"SELECT nombre FROM Servidor WHERE nombre=?",(args.servidor,))
    if not res.fetchone():
        print("No se ha encontrado el servidor")
    else:
        if not args.alumno:
            print(f"Eliminando los datos de inicio de sesión en el servidor {args.servidor}")
            cursor.execute(f"DELETE FROM IniciosSesion WHERE servidor=?",(args.servidor,))
        elif args.alumno in user_get():
            print(f"Eliminando los datos de inicio de sesión del alumno {args.alumno} en el servidor {args.servidor}")
            cursor.execute(f"DELETE FROM IniciosSesion WHERE servidor=? AND alumno=?",(args.servidor,args.alumno))
        else:
            print("No se encontró al alumno especificado")
        con.commit()
        cursor.close()
        con.close()


def comprueba_comandos(args):
    if not args.alumno:
        print("Buscando comandos para todos los alumnos")
        for alumno in user_get():
            for tar in buscalogs(alumno):
                leetar(alumno, tar)
    else:
        try:
            con = sqlite3.connect("/var/iso-student-analyzer/bbdd.db")
            cursor = con.cursor()
            res = cursor.execute(f"SELECT usuario FROM Alumno WHERE usuario=?;",(args.alumno,))
            if not res.fetchone():
                print(f"No se encontró el alumno {args.alumno}")
            else:
                print(f"Buscando nuevos logs para el alumno {args.alumno}")
                for tar in buscalogs(args.alumno):
                    leetar(args.alumno, tar)
        except:
            print("Ha ocurrido algún error")
        finally:
            cursor.close()
            con.close()

def nuevo_comando(args):
    comando = args.comando
    try:
        con = sqlite3.connect("/var/iso-student-analyzer/bbdd.db")
        cursor = con.cursor()
        res = cursor.execute(f"SELECT nombre FROM Comando WHERE nombre=?;",(comando,))
        if res.fetchone():
            print(f"El comando {comando} ya está en la Base de Datos")
        else:
            res = cursor.execute(f"INSERT INTO Comando VALUES(?);",(comando,))
            con.commit()
            for alumno in user_get_bbdd():
                res = cursor.execute(f"INSERT INTO AlumnoComando VALUES(?,?,0);",(alumno,comando,))
                con.commit()
                cuenta = 0
                tars = [ tarfichero for tarfichero in os.listdir(f"/home/{alumno}/sesiones/") if tarfichero.endswith(".tar") ]

                for tar in tars:
                    empaquetado = tarfile.open(name=f"/home/{alumno}/sesiones/{tar}",mode='r:',errorlevel=0)
                    for fichero in empaquetado:
                        if fichero.name != ".":
                            f = empaquetado.extractfile(fichero)
                            try:
                                cont = f.read()
                                contenido = cont.decode("utf-8")
                                cuenta = cuenta + contenido.count(comando)
                            except:
                                print(f"{fichero} no pudo leerse")

                if cuenta > 0:
                    cursor.execute(f"UPDATE AlumnoComando SET veces=? WHERE alumno=? AND comando=?;",(cuenta,alumno,comando,))
                    con.commit()
        con.commit()

    except:
        print("Ha ocurrido un error al añadir el comando")
    finally:
        cursor.close()
        con.close()


def borrar_comando(args):
    comando = args.comando
    con = sqlite3.connect("/var/iso-student-analyzer/bbdd.db")
    cursor = con.cursor()
    cursor.execute("PRAGMA foreign_keys = ON")
    res = cursor.execute(f"SELECT nombre FROM Comando WHERE nombre=?;",(comando,))
    if not res.fetchone():
        print(f"El comando {comando} no se encuentra en la Base de Datos")
    else:
        try:
            res = cursor.execute(f"DELETE FROM Comando WHERE nombre=?;",(comando,))
            con.commit()
        except:
            print("Ha ocurrido un error al eliminar el comando.")
        finally:
            cursor.close()
            con.close()

def reset_comandos(args):
    con = sqlite3.connect("/var/iso-student-analyzer/bbdd.db")
    cursor = con.cursor()
    try:
        if not args.alumno:
            cursor.execute(f"UPDATE AlumnoComando SET veces=0;")
            cursor.execute(f"UPDATE Alumno SET ultlog=NULL;")
        else:
            cursor.execute(f"UPDATE AlumnoComando SET veces=0 WHERE alumno=?;",(args.alumno,))
            cursor.execute(f"UPDATE Alumno SET ultlog=NULL WHERE usuario=?;",(args.alumno,))
        con.commit()
    except:
        if not args.alumno:
            print("Ha ocurrido algún error")
        else:
            print(f"Ha ocurrido algún error, ¿el alumno {args.alumno} existe?")



# DEFINICION INTERFAZ CLI
def get_cli():
    # Raíz y subparser del que cuelga todo
    cli = ArgumentParser()
    subcli = cli.add_subparsers(title='Comandos', required=True)


    # Comando daily
    daily_parser = subcli.add_parser('daily', help='Ejecución diaria')
    daily_parser.set_defaults(func=rutina_diaria)


    # Comando alumn
    alumn_parser = subcli.add_parser('alumn', help='Administrar alumnos')
    alumn_subparser = alumn_parser.add_subparsers(title='Administrar alumnos', required=True)
    alumn_find_parser = alumn_subparser.add_parser('find', help='Buscar nuevos alumnos en el sistema')
    alumn_find_parser.set_defaults(func=busca_alumnos)
    alumn_delete_parser = alumn_subparser.add_parser('delete', help='Borrar un alumno de la Base de Datos')
    alumn_delete_parser.add_argument('alumno', help='Nombre del alumno a eliminar')
    alumn_delete_parser.set_defaults(func=borrar_alumno)


    # Comando ej
    ejs_parser = subcli.add_parser('ej', help='Administrar ejercicios')
    ejs_subparser = ejs_parser.add_subparsers(title='Administra los ejercicios', required=True)
    ej_find_parser = ejs_subparser.add_parser('find', help='Buscar o actualizar ejercicios')
    ej_find_parser.set_defaults(func=busca_ejs)
    ej_check_parser = ejs_subparser.add_parser('check', help='Corregir ejercicios')
    ej_check_parser.add_argument('ejercicio', nargs='?', help='Nombre del ejercicio a corregir. Si no se pasa corrige todos los multiusuario.')
    ej_check_parser.add_argument('alumno', nargs='?', help='Nombre del alumno a corregir. Si no se pasa corrige a todos.')
    ej_check_parser.set_defaults(func=corrige_ejs)
    ej_delete_parser = ejs_subparser.add_parser('delete', help='Borrar un ejercicio')
    ej_delete_parser.add_argument('ejercicio', default='all', help='Nombre del ejercicio a eliminar.')
    ej_delete_parser.set_defaults(func=borrar_ejercicio)
    ej_reset_parser = ejs_subparser.add_parser('reset', help='Reinicia los las notas de uno o todos los alumnos')
    ej_reset_parser.add_argument('ejercicio', nargs='?', help='Ejercicio al que reiniciar correcciones')
    ej_reset_parser.add_argument('alumno', nargs='?', help='Alumno al que reiniciar correcciones')
    ej_reset_parser.set_defaults(func=reset_ej)


    # Comando server
    server_parser = subcli.add_parser('server', help='Administrar servidores')
    server_subparser = server_parser.add_subparsers(title='Administrar servidores', required=True)
    server_find_parser = server_subparser.add_parser('find', help='Buscar nuevos servidores')
    server_find_parser.set_defaults(func=busca_servidores)
    server_delete_parser = server_subparser.add_parser('delete', help='Borrar un servidor')
    server_delete_parser.add_argument('servidor', help='Nombre del servidor a eliminar')
    server_delete_parser.set_defaults(func=borrar_servidor)


    # Comando logins
    logins_parser = subcli.add_parser('logins', help='Administrar inicios de sesión')
    logins_subparser = logins_parser.add_subparsers(title='Administrar inicios de sesión', required=True)
    logins_check_parser = logins_subparser.add_parser('check', help='Escanear inicios de sesión')
    logins_check_parser.set_defaults(func=inicios_check)
    logins_check_parser.add_argument('servidor', nargs='?', help='Servidor en el que escanear. Si no se especifica, corrige en todos los multiusuario')
    logins_check_parser.add_argument('alumno', nargs='?', help='Alumno que escanear, si no se especifica, busca para todos')
    logins_delete_parser = logins_subparser.add_parser('delete', help='Borrar inicios de sesión')
    logins_delete_parser.set_defaults(func=inicios_borrar)
    logins_delete_parser.add_argument('servidor', help='servidor en el que eliminar los inicios de sesión')
    logins_delete_parser.add_argument('alumno', nargs='?', help='alumno del que eliminar los registros')


    # Comando password
    password_parser = subcli.add_parser('password', help='Administrar contraseñas web')
    password_subparser = password_parser.add_subparsers(title='Administrar contraseñas web', required=True)
    password_show_parser = password_subparser.add_parser('show', help='Mostrar una contraseña')
    password_show_parser.add_argument('promocion', help='Promoción de la que mostrar la contraseña')
    password_show_parser.set_defaults(func=muestra_contrasena)
    password_add_parser = password_subparser.add_parser('add', help='Cambiar una contraseña')
    password_add_parser.add_argument('promocion', help='Promocion a la que cambiar la contraseña')
    password_add_parser.add_argument('contraseña', nargs='?', help='Contraseña a asignar a la promoción')
    password_add_parser.set_defaults(func=cambiar_contrasena)
    password_delete_parser = password_subparser.add_parser('delete', help='Borrar una contraseña')
    password_delete_parser.add_argument('promocion', help='Promocion de la que borrar la contraseña')
    password_delete_parser.set_defaults(func=borrar_contrasena)


    # Comando command
    command_parser = subcli.add_parser('command', help='Administra los comandos a buscar')
    command_subparser = command_parser.add_subparsers(title='Administra los comandos a buscar', required=True)

    command_check_parser = command_subparser.add_parser('check', help='Comprueba los comandos ejecutados por los alumnos')
    command_check_parser.add_argument('alumno', nargs='?', help='Alumno al que escanear nuevos logs de comandos')
    command_check_parser.set_defaults(func=comprueba_comandos)

    command_add_parser = command_subparser.add_parser('add', help='Añade un nuevo comando')
    command_add_parser.add_argument('comando', help='comando que añadir')
    command_add_parser.set_defaults(func=nuevo_comando)

    command_delete_parser = command_subparser.add_parser('delete', help='Borra un comando de la lista de búsqueda')
    command_delete_parser.add_argument('comando', help='Comando que eliminar')
    command_delete_parser.set_defaults(func=borrar_comando)
    
    command_reset_parser = command_subparser.add_parser('reset', help='Reinicia los comandos ejecutados por uno o todos los alumnos')
    command_reset_parser.add_argument('alumno', nargs='?', help='Alumno al que reiniciar el registro de comandos')
    command_reset_parser.set_defaults(func=reset_comandos)
    

    # Comando reset
    reset_parser = subcli.add_parser('reset', help='Recrear la Base de Datos')
    reset_parser.add_argument('force', nargs='?')
    reset_parser.set_defaults(func=reset)


    return cli


if __name__ == "__main__":
    cli = get_cli()
    args = cli.parse_args()
    args.func(args)
