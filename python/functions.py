import os, yaml, sqlite3, tarfile, paramiko, datetime, utmp
from pathlib import Path

# Este script será llamado solo en la instalación.
def bbdd_creation():
	if not os.path.isfile("/var/iso-student-analyzer/bbdd.db"):
		con = sqlite3.connect("/var/iso-student-analyzer/bbdd.db")
		cursor = con.cursor()
		try:
			print("Generando estructura de la BBDD...")
			cursor.execute("CREATE TABLE Alumno(usuario VARCHAR(7) PRIMARY KEY NOT NULL,ultlog VARCHAR(50));")
			cursor.execute("CREATE TABLE Servidor(nombre VARCHAR(30) NOT NULL PRIMARY KEY, monouser BOOLEAN DEFAULT FALSE NOT NULL, usuario VARCHAR(30), direccion VARCHAR(15), certificado VARCHAR(100))")
			cursor.execute("CREATE TABLE Ejercicio(nombre VARCHAR(30) PRIMARY KEY NOT NULL, servidor VARCHAR(30), script VARCHAR(100) NOT NULL, FOREIGN KEY (servidor) REFERENCES Servidor(nombre) ON DELETE CASCADE);")
			cursor.execute("CREATE TABLE Comando(nombre VARCHAR(20) PRIMARY KEY NOT NULL);")
			cursor.execute("CREATE TABLE AlumnoComando(alumno VARCHAR(7) NOT NULL,comando VARCHAR(20) NOT NULL,veces INT NOT NULL DEFAULT 0,FOREIGN KEY (alumno) REFERENCES Alumno(usuario) ON DELETE CASCADE,FOREIGN KEY (comando) REFERENCES Comando(nombre) ON DELETE CASCADE,PRIMARY KEY(alumno, comando));")
			cursor.execute("CREATE TABLE AlumnoEjercicio(alumno VARCHAR(7) NOT NULL,ejercicio VARCHAR(20) NOT NULL,nota TINYINT CHECK((nota >= 0) AND (nota <= 10)) NOT NULL DEFAULT '0', comentario TEXT,FOREIGN KEY (alumno) REFERENCES Alumno(usuario) ON DELETE CASCADE,FOREIGN KEY (ejercicio) REFERENCES Ejercicio(nombre) ON DELETE CASCADE,PRIMARY KEY(alumno, ejercicio));")
			cursor.execute("CREATE TABLE IniciosSesion(momento DATETIME NOT NULL,alumno VARCHAR(7) NOT NULL,servidor VARCHAR(30) NOT NULL,duracion INT NOT NULL,PRIMARY KEY(momento,alumno,servidor),FOREIGN KEY (alumno) REFERENCES Alumno(usuario) ON DELETE CASCADE,FOREIGN KEY (servidor) REFERENCES Servidor(nombre) ON DELETE CASCADE);")
			cursor.execute("CREATE TABLE ServidorMono(servidor VARCHAR(30) NOT NULL, alumno VARCHAR(7) NOT NULL, usuario VARCHAR(30) NOT NULL, direccion VARCHAR(15) NOT NULL, certificado VARCHAR(100) NOT NULL, FOREIGN KEY (alumno) REFERENCES Alumno(usuario) ON DELETE CASCADE,FOREIGN KEY (servidor) REFERENCES Servidor(nombre) ON DELETE CASCADE,PRIMARY KEY(alumno, servidor));")
			cursor.execute("CREATE TABLE Accesos(promocion INT NOT NULL, clave VARCHAR(128));")
			con.commit()
			cursor.execute("INSERT INTO Comando(nombre) VALUES('ls'),('cat'),('find'),('sed'),('cd'),('awk'),('useradd'),('userdel'),('usermod'),('cut'),('grep'),('less'),('more'),('nano'),('vi'),('cp'),('pwd'),('mkdir'),('rm'),('sort'),('mv'),('|'),('tail'),('head'),('clear'),('touch'),('wc'),('echo'),('ln'),('who'),('date'),('chmod'),('ps'),('tar'),('scp'),('gzip'),('bzip2'),('xz'),('nl'),('file'),('>'),('<'),('wget'),('curl'),('tree'),('man'),('systemctl')")
			cursor.execute("INSERT INTO Accesos(promocion) VALUES(2022),(2023),(2024),(2025),(2026),(2027),(2028),(2029),(2030),(2031),(2032),(2033),(2034),(2035),(2036),(2037),(2038),(2039);")
			con.commit()
			print("Base de Datos Creada correctamente")
		except:
			print("Ha ocurrido un error al generar la estructura de la BBDD.")
		finally:
			cursor.close()
			con.close()
	else:
		print("La BBDD Ya existe.")

# ---------------------------------- BLOQUE USUARIOS ----------------------------------

# Devuelve el nombre de los usuarios válidos para el programa.
def user_get():
	passwd = os.popen('cat /etc/passwd | grep "/bin/bash" | cut -d: -f1 | grep -E "20(2[2-9]|3[0-9])\.[0-9]?[0-9]"').read()
	return passwd.splitlines()

# Devuelve los usuarios de la BBDD
def user_get_bbdd():
	con = sqlite3.connect("/var/iso-student-analyzer/bbdd.db")
	cursor = con.cursor()
	res = cursor.execute(f"SELECT usuario FROM Alumno")
	alumnos_old = res.fetchall()
	alumnos = []
	for i in range(0,len(alumnos_old)):
		alumnos.append(alumnos_old[i][0])
	return alumnos

# MIRAR SI HACER QUE AÑADA EL USUARIO QUE SE LE PASE O TODOS COMO AHORA.
# Este script será llamado en la instalación y cada día para comprobar si existen nuevos usuarios.
def user_add():
	usuarios = user_get()
	for usuario in usuarios:
		con = sqlite3.connect("/var/iso-student-analyzer/bbdd.db")
		cursor = con.cursor()
		cursor.execute("PRAGMA foreign_keys = ON")
		res = cursor.execute(f"SELECT * FROM Alumno WHERE usuario=?;",(usuario,))
		if not res.fetchone():
			try:
				cursor.execute(f"INSERT INTO Alumno(usuario) VALUES (?);",(usuario,))
				con.commit()

				# Genera una entrada por cada comando para el alumno
				res = cursor.execute(f"SELECT nombre FROM Comando;")
				comandos = res.fetchall()
				for comando in comandos:
					res = cursor.execute(f"INSERT INTO AlumnoComando VALUES(?,?,0);",(usuario, comando[0],))
				con.commit()
				for multi_ej in ej_get_multi_all():
					res = cursor.execute(f"INSERT INTO AlumnoEjercicio VALUES(?,?,0,'Sin corregir.');",(usuario, multi_ej[0],))
				con.commit()
				for mono_ej in ej_get_mono_all():
					res = cursor.execute(f"INSERT INTO AlumnoEjercicio VALUES(?,?,0,'Sin corregir.');",(usuario, mono_ej,))
				con.commit()
				print(f"Usuario {usuario} añadido correctamente")
			except:
				print(f"Error al añadir el usuario {usuario}")
				res = cursor.execute(f"DELETE FROM Alumno WHERE usuario=?;",(usuario,))
				con.commit()
			finally:
				cursor.close()
				con.close()

# ---------------------------------- BLOQUE COMANDOS ----------------------------------

def buscalogs(alumno):
	con = sqlite3.connect("/var/iso-student-analyzer/bbdd.db")
	cursor = con.cursor()
	cursor.execute(f"SELECT ultlog FROM Alumno WHERE usuario=?;",(alumno,))
	ult = cursor.fetchone()[0]
	cursor.close()
	con.close()

	tars = []
	marcador = 0

	for tar in os.listdir(f"/home/{alumno}/sesiones/"):
		if tar.endswith(".tar") and (ult == None or marcador == 1):
			tars.append(tar)
			if tar == ult:
				marcador = 1
	tars.sort()
	return tars


def leetar(alumno, tar):
	con = sqlite3.connect("/var/iso-student-analyzer/bbdd.db")
	cursor = con.cursor()
	res = cursor.execute(f"SELECT comando, veces FROM AlumnoComando WHERE alumno=?;",(alumno,))
	comandos_tupla = res.fetchall()
	comandos_alumno = []
	for i in range(0,len(comandos_tupla)):
		comandos_alumno.append(list(comandos_tupla[i]))
	comandos_tupla = None

	empaquetado = tarfile.open(name=f"/home/{alumno}/sesiones/{tar}",mode='r:',errorlevel=0)
	for fichero in empaquetado:
		if fichero.name != ".":
			f = empaquetado.extractfile(fichero)
			try:
				cont = f.read()
				contenido = cont.decode("utf-8")
				for i in range(0,len(comandos_alumno)):
					comandos_alumno[i][1] = comandos_alumno[i][1] +  contenido.count(f"{comandos_alumno[i][0]}")
			except:
				print(f"El fichero {fichero}en el tar {tar} no pudo leerse, es posible que contenga un carácter no procesable.")

	for i in range(0,len(comandos_alumno)):
		cursor.execute(f"UPDATE AlumnoComando SET veces=? WHERE alumno=? AND comando=?;",(comandos_alumno[i][1],alumno,comandos_alumno[i][0],))
		con.commit()
	cursor.execute(f"UPDATE Alumno SET ultlog=? WHERE usuario=?;",(tar,alumno,))
	con.commit()
	cursor.close()
	con.close()

# ---------------------------------- BLOQUE SERVIDORES ----------------------------------

# Función que permite ejecutar comandos sobre un servidor.
def server_conection(user,ip,cert,command):
    pkey = paramiko.RSAKey.from_private_key_file(cert)
    client = paramiko.SSHClient()
    policy = paramiko.AutoAddPolicy()
    client.set_missing_host_key_policy(policy)
    client.connect(ip, username=user, pkey=pkey)

    _stdin, stdout, _stderr = client.exec_command(command)
    salida = stdout.read().decode()
    client.close()
    return salida.split("\n")

def server_get():
	servers = []
	for fichero in os.listdir("/etc/iso-student-analyzer/servers/"):
		if fichero.endswith(".yaml"):
			fichero = Path(f"/etc/iso-student-analyzer/servers/{fichero}").stem
			servers.append(fichero)
	return servers

def server_add(server):
	with open(f"/etc/iso-student-analyzer/servers/{server}.yaml") as fich:
		ejdef = yaml.safe_load(fich)
		if all(clave in ejdef for clave in ('name', 'type', 'login')):
			if ejdef['name'] == None or ejdef['type'] == None or ejdef['login'] == None:
				print(f"Alguno de los campos del servidor {server} está vacío.")
			else:
				# MULTIUSER
				if ejdef['type'] == "multiuser":
					entrada =  ejdef.get('login', [])[0]
					if entrada['user'] == None or entrada['address'] == None or entrada['cert'] == None:
						print(f"Faltan datos en la definición del servidor {server}")
						
					else:
						con = sqlite3.connect("/var/iso-student-analyzer/bbdd.db")
						cursor = con.cursor()
						srv_fich = (ejdef['name'], 0, entrada['user'], entrada['address'], entrada['cert'])
						cursor.execute(f"SELECT nombre, monouser, usuario, direccion, certificado FROM Servidor WHERE nombre=?;",(srv_fich[0],))
						result = cursor.fetchone()
						if not result:
							try:
								cursor.execute(f"INSERT INTO Servidor VALUES (?,?,?,?,?);",(srv_fich[0],srv_fich[1],srv_fich[2],srv_fich[3],srv_fich[4],))
								con.commit()
								print(f"Servidor {srv_fich[0]} añadido correctamente")
							except:
								print(f"Error al añadir el servidor {srv_fich[0]} a la BBDD.")
							finally:
								cursor.close()
								con.close()
						else:
							if result != srv_fich:
								try:
									cursor.execute(f"UPDATE Servidor SET usuario=?,direccion=?,certificado=? WHERE nombre=?;",(srv_fich[2],srv_fich[3],srv_fich[4],srv_fich[0],))
									con.commit()
									print(f"Información del servidor {srv_fich[0]} actualizada correctamente")
								except:
									print(f"Error al actualizar la informacion del servidor {srv_fich[0]}.")
								finally:
									cursor.close()
									con.close()
				# MONOUSER
				elif ejdef['type'] == "monouser":
					alumnos = user_get()
					for entrada in ejdef.get('login', []):
						if all(i in entrada for i in ('student', 'user', 'address', 'cert')):
							if entrada['student'] == None or entrada['user'] == None or entrada['address'] == None or entrada['cert'] == None:
								print(f"Encontrado alumno mal definido en en el servidor {server}")
								
							elif str(entrada['student']) in alumnos:
								con = sqlite3.connect("/var/iso-student-analyzer/bbdd.db")
								cursor = con.cursor()
								srv_fich = (ejdef['name'], f"{str(entrada['student'])}", str(entrada['user']), entrada['address'], entrada['cert'])
								cursor.execute(f"SELECT nombre FROM Servidor WHERE nombre=? AND monouser=TRUE;",(srv_fich[0],))
								result = cursor.fetchone()
								cursor.close()
								con.close()
								if not result:
									try:
										con = sqlite3.connect("/var/iso-student-analyzer/bbdd.db")
										cursor = con.cursor()
										cursor.execute(f"INSERT INTO Servidor(nombre,monouser) VALUES (?, 1);",(srv_fich[0],))
										con.commit()
										print(f"Servidor {srv_fich[0]} añadido correctamente")
										cursor.execute(f"INSERT INTO ServidorMono VALUES (?,?,?,?,?);",(srv_fich[0],srv_fich[1],srv_fich[2],srv_fich[3],srv_fich[4],))
										con.commit()
										print(f"Alumno {srv_fich[1]} añadido al servidor {srv_fich[0]}.")

									except:
										print(f"Fallo al añadir el servidor {srv_fich[0]}.")
									finally:
										cursor.close()
										con.close()
								else:
									con = sqlite3.connect("/var/iso-student-analyzer/bbdd.db")
									cursor = con.cursor()
									cursor.execute(f"SELECT * FROM ServidorMono WHERE servidor=? AND alumno=?;",(srv_fich[0],srv_fich[1],))
									result = cursor.fetchone()
									if not result:
										try:
											cursor.execute(f"INSERT INTO ServidorMono VALUES (?,?,?,?,?);",(srv_fich[0],srv_fich[1],srv_fich[2],srv_fich[3],srv_fich[4]))
											con.commit()
											print(f"Alumno {srv_fich[1]} añadido al servidor {srv_fich[0]}.")
										except:
											print(f"Fallo al añadir el alumno {srv_fich[1]} a {srv_fich[0]}.")
										finally:
											cursor.close()
											con.close()
									else:
										if result != srv_fich:
											try:
												con = sqlite3.connect("/var/iso-student-analyzer/bbdd.db")
												cursor = con.cursor()
												cursor.execute(f"UPDATE ServidorMono SET usuario=?, direccion=?, certificado=? WHERE servidor=? AND alumno=?;",(srv_fich[2],srv_fich[3],srv_fich[4],srv_fich[0],srv_fich[1],))
												con.commit()
												print(f"Información del alumno {srv_fich[1]} en el servidor {srv_fich[0]} actualizada correctamente")
											except:
												print(f"Error al actualizar la información del alumno {srv_fich[1]} en el servidor {srv_fich[0]}")
											finally:
												cursor.close()
												con.close()

				else:
					print(f"La variable type del servidor {server} contiene un valor no soportado.")
		else:
			print(f"Faltan campos en la definición del servidor {server}.")

# Obtiene el nombre de todos los servidores multiusuario
def server_get_multi_all():
	con = sqlite3.connect("/var/iso-student-analyzer/bbdd.db")
	cursor = con.cursor()
	cursor.execute(f"SELECT nombre FROM Servidor WHERE monouser=FALSE;")
	multi_servers = cursor.fetchall()
	cursor.close()
	con.close()
	return multi_servers

# Obtiene el nombre de todos los servidores monousuario
def server_get_mono_all():
	con = sqlite3.connect("/var/iso-student-analyzer/bbdd.db")
	cursor = con.cursor()
	cursor.execute(f"SELECT nombre FROM Servidor WHERE monouser=TRUE;")
	mono_servers = cursor.fetchall()
	cursor.close()
	con.close()
	return mono_servers

# Devuelve la información de conexión a un servidor multiusuario dado
# Requiere el nombre de un servidor multiusuario
def alumn_server_get_multi(server):
	con = sqlite3.connect("/var/iso-student-analyzer/bbdd.db")
	cursor = con.cursor()
	cursor.execute(f"SELECT nombre, usuario, direccion, certificado FROM Servidor WHERE nombre=? AND monouser=FALSE;",(server[0],))
	multi_server = cursor.fetchall()[0]
	cursor.close()
	con.close()
	return multi_server

# Devuelve las entradas de todos los alumnos para un servidor monousuario dado.
# Requiere el nombre de un servidor monousuario
def alumn_server_get_mono(server):
	hoy = datetime.date.today()
	ayer = hoy - datetime.timedelta(days=1)
	con = sqlite3.connect("/var/iso-student-analyzer/bbdd.db")
	cursor = con.cursor()
	cursor.execute(f"SELECT servidor, alumno, ServidorMono.usuario, ServidorMono.direccion, ServidorMono.certificado FROM ServidorMono LEFT JOIN Servidor ON Servidor.nombre=ServidorMono.servidor WHERE servidor=?;",(server,))
	mono_server = cursor.fetchall()
	cursor.close()
	con.close()
	return mono_server

# ---------------------------------- BLOQUE LOGINS ----------------------------------

# Calcula los inicios de sesión del alumno en un servidor según la entrada dada.
# Requiere la info de conexión de un alumno para un servidor

def alumn_server_calc_mono(entrada):
	server = entrada[0]
	alumn = entrada[1]
	user = entrada[2]
	ip = entrada[3]
	cert = entrada[4]
	if os.popen(f'ping -c 3 {ip} >/dev/null;echo $?').read()[0] == "0":
		pkey = paramiko.RSAKey.from_private_key_file(cert)
		client = paramiko.SSHClient()
		policy = paramiko.AutoAddPolicy()
		client.set_missing_host_key_policy(policy)
		client.connect(ip, username=user, pkey=pkey)
		sftp = client.open_sftp()
		ficheros_todos = sftp.listdir('/var/log')
		ficheros_logs = []
		ficheros_logs = [log_todos for log_todos in ficheros_todos if log_todos.startswith("wtmp") or log_todos.startswith("utmp")]
		for fichero_log in ficheros_logs:
			fichero = sftp.file(f"/var/log/{fichero_log}")
			buf = fichero.read()
			con = sqlite3.connect("/var/iso-student-analyzer/bbdd.db")
			cursor = con.cursor()
			cursor.execute(f"SELECT momento,'{user}',servidor FROM IniciosSesion WHERE servidor=? AND alumno=?;",(server,alumn))
			res = cursor.fetchall()
			cursor.execute(f"SELECT MAX(momento) FROM IniciosSesion WHERE servidor=? AND alumno=?;",(server,alumn))
			min_num = cursor.fetchone()[0]
			if min_num == None:
				min_num = 0
			dic = {}
			for linea in utmp.read(buf):
				if str(linea.type) == "UTmpRecordType.user_process" and linea.sec > min_num and linea.user == user:
					if (linea.sec,linea.user,server) not in res:
						dic[linea.pid] = (linea.sec,linea.user)
				if str(linea.type) == "UTmpRecordType.dead_process" and linea.pid in dic:
					segundos = linea.sec - dic[linea.pid][0]
					tiempo = int(segundos / 60)
					if int(tiempo == 0):
						tiempo = 1
					cursor.execute(f"INSERT INTO IniciosSesion VALUES(?,?,?,?);",(dic[linea.pid][0],alumn,server,tiempo))
					con.commit()
					dic.pop(linea.pid)
			cursor.close()
			con.close()
		client.close()


# Calcula los inicios de sesión de todos los alumnos del servidor dado.
# Requiere la info de conexión.

def alumn_server_calc_multi(servidor):
	server = servidor[0]
	user = servidor[1]
	ip = servidor[2]
	cert = servidor[3]
	if os.popen(f'ping -c 3 {ip} >/dev/null;echo $?').read()[0] == "0":
		pkey = paramiko.RSAKey.from_private_key_file(cert)
		client = paramiko.SSHClient()
		policy = paramiko.AutoAddPolicy()
		client.set_missing_host_key_policy(policy)
		client.connect(ip, username=user, pkey=pkey)
		sftp = client.open_sftp()
		ficheros_todos = sftp.listdir('/var/log')
		ficheros_logs = [log_todos for log_todos in ficheros_todos if log_todos.startswith("wtmp") or log_todos.startswith("utmp")]
		for fichero_log in ficheros_logs:
			fichero = sftp.file(f"/var/log/{fichero_log}")
			buf = fichero.read()
			usuarios = user_get()
			con = sqlite3.connect("/var/iso-student-analyzer/bbdd.db")
			cursor = con.cursor()
			cursor.execute(f"SELECT momento,alumno,servidor FROM IniciosSesion WHERE servidor=?;",(server,))
			res = cursor.fetchall()
			cursor.execute(f"SELECT MAX(momento) FROM IniciosSesion WHERE servidor=?;",(server,))
			min_num = cursor.fetchone()[0]
			if min_num == None:
				min_num = 0
			dic = {}
			for linea in utmp.read(buf):
				if str(linea.type) == "UTmpRecordType.user_process" and linea.sec > min_num and linea.user in usuarios:
					if (linea.sec,linea.user,server) not in res:
						dic[linea.pid] = (linea.sec,linea.user)
				if str(linea.type) == "UTmpRecordType.dead_process" and linea.pid in dic:
					segundos = linea.sec - dic[linea.pid][0]
					tiempo = int(segundos / 60)
					if int(tiempo == 0):
						tiempo = 1
					cursor.execute(f"INSERT INTO IniciosSesion VALUES(?,?,?,?);",(dic[linea.pid][0],dic[linea.pid][1],server,tiempo))
					con.commit()
					dic.pop(linea.pid)
			cursor.close()
			con.close()
		client.close()


# ---------------------------------- BLOQUE EJERCICIOS ----------------------------------

# Obtiene todos los ficheros de definición de ejercicio correctamente definidos y devuelve su informacion.
def ej_get_yaml():
	ejs = []
	for fichero in os.listdir("/etc/iso-student-analyzer/ej/"):
		if fichero.endswith(".yaml"):
			try:
				with open(f"/etc/iso-student-analyzer/ej/{fichero}") as fich:
					ejdef = yaml.safe_load(fich)
					if all(clave in ejdef for clave in ('name', 'server', 'script')):
						ejercicio = (f"{ejdef['name']}",f"{ejdef['server']}",f"{ejdef['script']}")
						if ejercicio[0] != None and ejercicio[1] != None and ejercicio[2] != None:
							ejs.append(ejercicio)
						else:
							print(f"Se encontraron campos obligatorios sin rellenar en el fichero de definición de ejercicio {fichero}")
					else:
						print(f"Se encontraron campos faltantes en el fichero de definición de ejercicio {fichero}")
			except:
				print(f"Error al abrir el fichero {fichero}")
	return ejs

# MODIFICAR


# Obtiene el nombre de todos los ejercicios multiusuario.
def ej_get_multi_all():
	ejs_bd = []
	con = sqlite3.connect("/var/iso-student-analyzer/bbdd.db")
	cursor = con.cursor()
	res = cursor.execute(f"SELECT Ejercicio.nombre FROM Ejercicio LEFT JOIN Servidor ON Servidor.nombre = Ejercicio.servidor WHERE monouser=FALSE;")
	res.fetchall
	for reg in res:
		ejs_bd.append(reg)
	cursor.close()
	con.close()
	return ejs_bd

# Obtiene el nombre de todos los ejercicios monousuario.
def ej_get_mono_all():
	ejs_name = []
	con = sqlite3.connect("/var/iso-student-analyzer/bbdd.db")
	cursor = con.cursor()
	res = cursor.execute(f"SELECT Ejercicio.nombre FROM Ejercicio LEFT JOIN Servidor ON Ejercicio.servidor=Servidor.nombre WHERE monouser=TRUE;")
	res.fetchall
	for reg in res:
		ejs_name.append(reg[0])
	cursor.close()
	con.close()
	return ejs_name

# Devuelve información de inicio de sesión al servidor pasado.
def ej_get_multi(nombre):
	con = sqlite3.connect("/var/iso-student-analyzer/bbdd.db")
	cursor = con.cursor()
	res = cursor.execute(f"SELECT Ejercicio.nombre, script, usuario, direccion, certificado FROM Ejercicio LEFT JOIN Servidor ON Ejercicio.servidor=Servidor.nombre WHERE monouser=FALSE AND Ejercicio.nombre=?;",(nombre,))
	ej_info = res.fetchall()
	cursor.close()
	con.close()
	return ej_info

# Devuelve información de inicio de sesión al servidor pasado.
def ej_get_mono(nombre):
	ejercicio_info = []
	con = sqlite3.connect("/var/iso-student-analyzer/bbdd.db")
	cursor = con.cursor()
	res = cursor.execute(f"SELECT Ejercicio.nombre, alumno, script, usuario, direccion, certificado FROM ServidorMono LEFT JOIN Ejercicio ON Ejercicio.servidor=ServidorMono.servidor WHERE Ejercicio.nombre=?;",(nombre,))
	res.fetchall
	for reg in res:
		ejercicio_info.append(reg)
	cursor.close()
	con.close()
	return ejercicio_info


# Intenta añadir a la BBDD el ejercicio que reciba.
def ej_add(ejercicio):
# Fichero bien definido
	con = sqlite3.connect("/var/iso-student-analyzer/bbdd.db")
	cursor = con.cursor()
	cursor.execute(f"SELECT nombre FROM Servidor WHERE nombre=?",(ejercicio[1],))
	result = cursor.fetchone()
	cursor.close()
	con.close()
	# Comprobamos si el servidor al que el ejercicio hace referencia existe.
	if not result:
		print(f"No se encontró ningún servidor con el nombre {ejercicio[1]}. No se pudo procesar el ejercicio {ejercicio[0]}.")
	else:
		con = sqlite3.connect("/var/iso-student-analyzer/bbdd.db")
		cursor = con.cursor()
		cursor.execute("PRAGMA foreign_keys = ON")
		cursor.execute(f"SELECT nombre, servidor, script FROM Ejercicio WHERE nombre=?;",(ejercicio[0],))
		result = cursor.fetchone()
		cursor.close()
		con.close()
		if result == None:
			try:
				con = sqlite3.connect("/var/iso-student-analyzer/bbdd.db")
				cursor = con.cursor()
				cursor.execute(f"INSERT INTO Ejercicio VALUES(?,?,?);",(ejercicio[0],ejercicio[1],ejercicio[2],))
				con.commit()
				for alumno in user_get_bbdd():
					cursor.execute(f"INSERT INTO AlumnoEjercicio VALUES(?,?,0,'Sin corregir.');",(alumno,ejercicio[0],))
					con.commit()
				print(f"Ejercicio {ejercicio[0]} añadido correctamente a la Base de Datos")
			except:
				print(f"Ha ocurrido un error al añadir el ejercicio {ejercicio[0]} a la BBDD.")
				cursor.execute(f"DELETE FROM Ejercicio WHERE nombre=?;",(ejercicio[0],))
				con.commit()
			finally:
				cursor.close()
				con.close()

		elif result != ejercicio:
			# Si se detectan diferencias entre el fichero actual y el ejercicio en la BBDD se actualiza el de la BBDD.
			try:
				con = sqlite3.connect("/var/iso-student-analyzer/bbdd.db")
				cursor = con.cursor()
				cursor.execute(f"UPDATE Ejercicio SET servidor=?,script=? WHERE nombre=?;",(ejercicio[1],ejercicio[2],ejercicio[0],))
				con.commit()
				print(f"Ejercicio {ejercicio[0]} actualizado correctamente")
			except:
				print(f"Error al actualizar la informacion del ejercicio {ejercicio[0]}.")
			finally:
				cursor.close()
				con.close()

def server_conection_ej(user,ip,cert,alumno,script):
	pkey = paramiko.RSAKey.from_private_key_file(cert)
	client = paramiko.SSHClient()
	policy = paramiko.AutoAddPolicy()
	client.set_missing_host_key_policy(policy)
	client.connect(ip, username=user, pkey=pkey)
	stdin, stdout, _stderr = client.exec_command(f"/bin/bash -c 'export alumn={alumno}; {script}'", timeout=25)
	salida = stdout.read().decode()
	return salida.split("\n")

def ej_exec_multi(ej):
	nombre = ej[0]
	script = ej[1]
	usuario = ej[2]
	direccion = ej[3]
	certificado = ej[4]
	
	if os.path.isfile(script) and os.popen(f'ping -c 3 {direccion} >/dev/null;echo $?').read()[0] == "0":
		with open(f"{script}", "r", encoding='utf-8') as script_text:
			script_content = script_text.read()
		con = sqlite3.connect("/var/iso-student-analyzer/bbdd.db")
		cursor = con.cursor()
		for alumno in user_get():
			cursor.execute(f"SELECT nota FROM AlumnoEjercicio WHERE ejercicio=? AND alumno=?;",(nombre,alumno,))
			nota_vieja_pre = cursor.fetchone()
			if not nota_vieja_pre:
				nota_vieja = 0
				cursor.execute(f"INSERT INTO AlumnoEjercicio VALUES(?,?,?);",(alumno,nombre,nota_vieja,))
				con.commit()
			else:
				nota_vieja = nota_vieja_pre[0]
			if nota_vieja != 10:
				# Ejecución del script
				salida = server_conection_ej(usuario,direccion,certificado,alumno,script_content)
				nota_nueva = int(salida[0])
				if nota_nueva >= nota_vieja and nota_nueva <=10 and nota_nueva >=0 and nota_nueva != None:
					comentario = ""
					for linea in salida[1:]:
						comentario = comentario + linea + "\n"
					try:
						cursor.execute(f"UPDATE AlumnoEjercicio SET nota=?, comentario=? WHERE alumno=? AND ejercicio=?;",(salida[0],comentario,alumno,nombre,))
						con.commit()
					except:
						print(f"Error al actualizar la nota de {alumno} en el ejercicio {nombre}.")
		cursor.close()
		con.close()


def ej_exec_mono(ej):
	nombre = ej[0]
	alumno = ej[1]
	script = ej[2]
	usuario = ej[3]
	direccion = ej[4]
	certificado = ej[5]

	if os.path.isfile(script) and os.popen(f'ping -c 3 {direccion} >/dev/null;echo $?').read()[0] == "0":
		with open(f"{script}", "r", encoding='utf8') as script_text:
			script_content = script_text.read()
			con = sqlite3.connect("/var/iso-student-analyzer/bbdd.db")
			cursor = con.cursor()
			cursor.execute(f"SELECT nota FROM AlumnoEjercicio WHERE ejercicio=? AND alumno=?;",(nombre,alumno,))
			nota_vieja_pre = cursor.fetchone()
			if not nota_vieja_pre:
				nota_vieja = 0
				cursor.execute(f"INSERT INTO AlumnoEjercicio VALUES(?,?,?);",(alumno,nombre,nota_vieja,))
				con.commit()
			else:
				nota_vieja = nota_vieja_pre[0]
			if nota_vieja != 10:
				# Ejecución del script
				salida = server_conection_ej(usuario,direccion,certificado,alumno,script_content)
				nota_nueva = int(salida[0])
				if nota_nueva >= nota_vieja and nota_nueva <=10 and nota_nueva >=0 and nota_nueva != None:
					comentario = ""
					for linea in salida[1:]:
						comentario = comentario + linea + "\n"
					try:
						cursor.execute(f"UPDATE AlumnoEjercicio SET nota=?, comentario=? WHERE alumno=? AND ejercicio=?;",(salida[0],comentario,alumno,nombre,))
						con.commit()
					except:
						print(f"Error al actualizar la nota de {alumno} en el ejercicio {nombre}.")
		cursor.close()
		con.close()



# FUNCIONES COMBINADAS

def get_servers():
	for server in server_get():
		server_add(server)

def get_multi_logins():
	for server in server_get_multi_all():
		alumn_server_calc_multi(alumn_server_get_multi(server))

def get_multi_server_logins(server_name):
	alumn_server_calc_multi(alumn_server_get_multi(server_name))

def find_ejs():
	for ejercicio in ej_get_yaml():
		ej_add(ejercicio)

def ej_check_mono():
	for eje in ej_get_mono_all():
		ej_exec_mono(ej_get_mono(eje[0])[0])


def ej_check_multi():
	for eje in ej_get_multi_all():
		ej_exec_multi(ej_get_multi(eje[0])[0])