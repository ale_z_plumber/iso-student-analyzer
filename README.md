# ISO Student analyzer

Programa cuyo objetivo es analizar el trabajo de los Alumnos de primero ASIR en la asignatura ISO.
mostrará tiempo trabajado, comandos ejecutados y ejercicios superados.


## Proceso diario:
    - Se buscan nuevos alumnos que añadir.
    - Se buscan nuevos ejercicios definidos.
    - Se corrigen ejercicios siguiento el procedimiento.
    - Se obtienen los nuevos inicios de sesión.
    - Se comprueban los nuevos comandos ejecutados.

## Tareas por hacer

- [x] [Completar programa Python]()
  - [x] [Crear funciones de detección de usuarios]()
  - [x] [Crear funciones de detección de ejercicios]()
  - [x] [Crear funciones de corrección de ejercicios]()
  - [x] [Crear funciones de detección de servidores]()
  - [x] [Obtener inicios de sesión]()
  - [x] [Crear funciones de comprobación de comandos]()
  - [x] [Modificar BBDD para borrados en cascada]()
  - [x] [Crear interfaz cli de administración]()
- [x] [Completar página web]()
  - [x] [Crear estructura páginas php]()
  - [x] [Sesiones en la página web]()
  - [x] [Completar estilos página web]()
  - [x] [Diseño gráfico página web]()
- [x] [Empaquetado aplicación]()
  - [x] [Empaquetar aplicación Python]()
  - [x] [Crear rutina de cron]()
  - [x] [Conseguir desplegar servicio web automáticamente]()
  - [x] [crear script de instalación]()
- [x] [Mejora de funciones]()
  - [x] [Mejora de inicios de sesión]()
  - [x] [Crear funciones administrar comandos]()