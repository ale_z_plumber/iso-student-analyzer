#!/usr/bin/env bash
if [[ "$(id -u)" != 0 ]]; then
    echo 'Debes ejecutar este script como root';
    exit 1;
fi
echo "Comprobando e instalando las dependencias necesarias";
apt update && apt install -y acl python3 python3-pip lighttpd lighttpd-mod-openssl php-cgi php-sqlite3 ;
pip install pyinstaller pyaml argparse paramiko utmp
lighty-enable-mod fastcgi;
lighty-enable-mod fastcgi-php;
echo "Dependencias resultas, se van a crear el usuario y grupo necesarios.";
groupadd iso-sa;
useradd -d /var/iso-student-analyzer -s /sbin/nologin -g iso-sa iso-sa;
echo "Creando los directorios necesarios";
mkdir /var/iso-student-analyzer && mkdir -p /etc/iso-student-analyzer/{ej,scripts,servers,certs};
echo "Compilando programa python";
python3 -m PyInstaller --onefile python/iso-sa-cli.py;
echo "Colocando el binario en el sistema";
mv ./dist/iso-sa-cli /usr/bin/iso-sa-cli;
rm -rf ./dist/ && rm -rf ./build/ && rm iso-sa-cli.spec && rm -rf /var/www/html/*;
echo "Copiando página web";
cp -r web/* /var/www/html/;
echo "Preparando permisos";
chown -R iso-sa:iso-sa /etc/iso-student-analyzer /var/iso-student-analyzer /usr/bin/iso-sa-cli;
chown -R www-data:iso-sa /var/www/;
chmod -R 770 /etc/iso-student-analyzer /var/iso-student-analyzer;
chmod 770 /usr/bin/iso-sa-cli;
#sed -i 's/server.groupname.*= \"www-data\"/server.groupname            = \"iso-sa\"/g' /etc/lighttpd/lighttpd.conf;
usermod -aG www-data iso-sa;
usermod -aG iso-sa www-data;
setfacl -R -m g:iso-sa:rx /home/20[2,3][0-9]*
systemctl restart lighttpd;
echo "Creando Rutina cron";
echo "0 4 0 0 0 0 iso-sa iso-sa-cli daily" | tee /etc/cron.d/iso-sa_daily 1>/dev/null
echo "Instalación finalizada, por favor, añade un usuario al grupo iso-sa para poder utilizar el programa y ejecuta 'iso-sa-cli daily' con ese usuario";
exit;
